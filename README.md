# Concept Maps #

The further development of this plugin has been handed over to the IT-PFL Team of the RWTH Aachen University IT Center.
We are gratious that the work on plugin is continued and annouce hereby that all further improvements will be found in the new repository under [https://git.rwth-aachen.de/moodle_l2p_public/moodle-conceptmaps](https://git.rwth-aachen.de/moodle_l2p_public/moodle-conceptmaps).
The content of this repository here should be considered deprecated, it will be only kept to redirect possible references to the new repository.

## License ##

2019 LuFG I9 <rabea.degroot@rwth-aachen.de>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
