<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/conceptmaps/classes/controller/basic_controller.php');
require_once($CFG->dirroot.'/mod/conceptmaps/classes/enums.php');

class mod_conceptmaps_create_topic_controller extends mod_conceptmaps_basic_controller {

    /** @var bool States if the upload of the data to the database was successful */
    private $store_successful;

    /**
     * mod_conceptmaps_create_topic_controller constructor.
     * @param $cmid
     * @param $conceptmapsid
     */
    public function __construct($cmid, $conceptmapsid){
        parent::__construct($cmid, $conceptmapsid);
    }

    /**
     * Inserts the given json entries into the database
     *
     * @param $ls_data
     */
    private function store_created_topic_in_database($data){
        global $DB;
        $this->uploadSuccessful = true;

        $currentTime = new DateTime('now');

        try{
          $transaction = $DB->start_delegated_transaction();
          // Insert topic
          $newEntry = new stdClass();
          $newEntry->conceptmapsid = $this->conceptmapsid;
          $newEntry->name = $data->name;
          $newEntry->start = $data->start == 0 ? null : $data->start;
          $newEntry->end = $data->end == 0 ? null : $data->end;
          $newEntry->startcorrection = $data->startcorrection == 0 ? null : $data->startcorrection;
          $newEntry->endcorrection = $data->endcorrection == 0 ? null : $data->endcorrection;
          $newEntry->corrected = 0;
          //$newEntry->timecreated = $currentTime->getTimestamp();

          $topicid = $DB->insert_record('conceptmaps_topics', $newEntry);

          // Insert terms
          foreach($data->term as $key => $value) {
            if(!empty($value)) {
              $newTerm = new stdClass();
              $newTerm->conceptmapstopic = $topicid;
              $newTerm->name = $value;
              $newTerm->editable = 0;
              $newTerm->positionx = 0;
              $newTerm->positiony = 0;

              $DB->insert_record('conceptmaps_terms', $newTerm);
            }
          }

          // Insert empty Terms
          for($i = 0; $i < $data->count_empty_terms; $i++) {

            $newTerm = new stdClass();
            $newTerm->conceptmapstopic = $topicid;
            $newTerm->name = "";
            $newTerm->editable = 1;
            $newTerm->positionx = 0;
            $newTerm->positiony = 0;

            $DB->insert_record('conceptmaps_terms', $newTerm);
          }

          $transaction->allow_commit();
        }
        catch(Exception $e) {
            $this->uploadSuccessful = false;
            $transaction->rollback($e);
        }
    }

    private function store_edit_topic_in_database($data) {
      global $DB;
      $data->id = $data->topic;
      $data->start = $data->start == null ? "" : $data->start;
      $data->end = $data->end == null ? "" : $data->end;
      $data->startcorrection = $data->startcorrection == null ? "" : $data->startcorrection;
      $data->endcorrection = $data->endcorrection == null ? "" : $data->endcorrection;

      try{
        $transaction = $DB->start_delegated_transaction();
        $DB->update_record('conceptmaps_topics', $data, $bulk=false);

        //Delete all terms and then add the new ones
        $DB->delete_records('conceptmaps_terms', array('conceptmapstopic' => $data->id));

        // Insert terms
        foreach($data->term as $key => $value) {
          if(!empty($value)) {
            $newTerm = new stdClass();
            $newTerm->conceptmapstopic = $data->id;
            $newTerm->name = $value;
            $newTerm->editable = 0;
            $newTerm->positionx = 0;
            $newTerm->positiony = 0;

            $DB->insert_record('conceptmaps_terms', $newTerm);
          }
        }

        // Insert empty Terms
        for($i = 0; $i < $data->count_empty_terms; $i++) {

          $newTerm = new stdClass();
          $newTerm->conceptmapstopic = $data->id;
          $newTerm->name = "";
          $newTerm->editable = 1;
          $newTerm->positionx = 0;
          $newTerm->positiony = 0;

          $DB->insert_record('conceptmaps_terms', $newTerm);
        }

        $transaction->allow_commit();

        return true;
      } catch(Exception $e) {
        $transaction->rollback($e);
        return false;
      }

    }

    /**
     * Redirects to the main view (not student view) with a message about the successful upload of the
     * data
     */
    private function go_back_to_main_view(){
        $redirectionTarget = new moodle_url('/mod/conceptmaps/view.php', array('id'=>$this->cmid));
        redirect($redirectionTarget->out(), get_string('store_successful', 'conceptmaps'), null,
            \core\output\notification::NOTIFY_SUCCESS);
    }

    /**
     * Handle access to view
     */
    public function handle_access(){
        $context = context_module::instance($this->cmid);

        if(!has_capability('mod/conceptmaps:editsettings', $context)){

            $redirectionTarget = new moodle_url('/mod/conceptmaps/view.php', array('id'=>$this->cmid));
            redirect($redirectionTarget->out());
        }
    }

    /**
     * Handle cancel operation in the form
     */
    public function handle_cancel(){
        $overview = new moodle_url('/mod/conceptmaps/view.php', array('id'=>$this->cmid));
        redirect($overview->out());
    }

    /**
     * Checks if there is valid input submitted and triggers the upload of the data to the database
     *
     * @param $mform
     */
    public function handle_form($mform, $new){
      if($mform->is_cancelled()) {
        $this->handle_cancel();
      }
        if($fromform = $mform->get_data()){
            //Valid data was submitted
            if($this->checkData($fromform)){

              if($new) {
                $this->store_created_topic_in_database($fromform);
                if($this->uploadSuccessful){
                    $this->go_back_to_main_view();
                }
              } else {
                if($this->store_edit_topic_in_database($fromform)) {
                    $this->go_back_to_main_view();
                } else {
                  //TODO: Message for failing
                }
              }

            }
        }
    }



    private function checkData($data) {
      //TODO: check if dates are valid and make sense
      return true;
    }

    public function render_tabs() {
      $taburl = new moodle_url('/mod/conceptmaps/view.php', array('id' => $this->cmid));
      $modulecontext = context_module::instance($this->cmid);
      return $this->myrenderer->conceptmaps_render_tabs($taburl, Action::Topic_Overview, $modulecontext);
    }
}
