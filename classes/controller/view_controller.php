<?php
# @Date:   2019-11-07T14:19:23+01:00
# @Last modified time: 2019-11-07T15:41:15+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class mod_conceptmaps_basic_controller
 *
 * @package     mod_conceptmaps
 * @copyright   2018 Sven Judel <sven.judel@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/mod/conceptmaps/classes/controller/basic_controller.php');
require_once($CFG->dirroot.'/mod/conceptmaps/classes/enums.php');
require_once($CFG->dirroot.'/mod/conceptmaps/locallib.php');

class mod_conceptmaps_view_controller extends mod_conceptmaps_basic_controller {

  /**
   * mod_conceptmaps_overview_controller constructor.
   * @param $cmid
   * @param $conceptmapsid
   */
  public function __construct($cmid, $conceptmapsid){
      parent::__construct($cmid, $conceptmapsid);
  }

  /**
   * Handle access to view
   */
  public function handle_access(){
      $context = context_module::instance($this->cmid);

      if(!has_capability('mod/conceptmaps:editsettings', $context)){
          $redirectionTarget = new moodle_url('/mod/conceptmaps/view_student.php', array('id'=>$this->cmid));
          redirect($redirectionTarget->out());
      }
  }

  public function handle_action($action) {
    $taburl = new moodle_url('/mod/conceptmaps/view.php', array('id' => $this->cmid));
    $modulecontext = context_module::instance($this->cmid);
    $ret = $this->myrenderer->conceptmaps_render_tabs($taburl, $action, $modulecontext);
    switch($action) {
      case Action::Topic_Overview:
        $ret .= $this->handle_topic_overview();
        return $ret;
      case Action::Submissions:
        $ret .= $this->handle_submissions();
        return $ret;
      case Action::Autocorrection:
        $ret .= $this->handle_autocorrection();
        return $ret;
      case Action::Delete:
        $ret .= $this->handle_topic_delete();
        return $ret;
      case Action::SubmissionsTopic:
        $ret .= $this->handle_submission_topic();
        return $ret;
      case Action::Submission:
        $ret .= $this->handle_solution(false);
        return $ret;
      case Action::Solution:
        $ret .= $this->handle_solution(true);
        return $ret;
      case Action::SingleCorrection:
        $ret .= $this->handle_single_correction();
        return $ret;
      case Action::CorrectConceptmap:
        $ret .= $this->handle_correct_conceptmap();
        return $ret;
      default:
        $ret = $this->myrenderer->conceptmaps_render_tabs($taburl, Action::Topic_Overview, $modulecontext);
        $ret .= $this->handle_topic_overview();
        return $ret;
    }
  }

  private function handle_topic_overview() {
    global $CFG;
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/topic_overview.php');
    return $this->myrenderer->render_topic_overview(new topic_overview($this->cmid, $this->conceptmapsid));
  }

  private function handle_topic_delete() {
    global $CFG, $DB;
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/topic_overview.php');
    $topicid = required_param('topic', PARAM_INT);
    delete_topic($topicid);
    return $this->myrenderer->render_topic_overview(new topic_overview($this->cmid, $this->conceptmapsid));
  }

  /**
   * Redirects to the main view (not student view) with a message about the successful upload of the
   * data
   */
  private function go_back_to_main_view(){
      $redirectionTarget = new moodle_url('/mod/conceptmaps/view.php', array('id'=>$this->cmid));
      redirect($redirectionTarget->out(), get_string('store_successful', 'conceptmaps'), null,
          \core\output\notification::NOTIFY_SUCCESS);
  }

  private function handle_submissions() {
    global $CFG;
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/submissions.php');
    return $this->myrenderer->render_submissons(new submissions($this->cmid, $this->conceptmapsid));
  }

  private function handle_submission_topic() {
    global $CFG;
    $topic = required_param('topic', PARAM_INT);
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/submissionsTopic.php');
    return $this->myrenderer->render_submissons_topic(new submissionsTopic($this->cmid, $topic));
  }

  private function handle_single_correction() {
    global $CFG, $DB;
    $topic = required_param('topic', PARAM_INT);
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/singleCorrection.php');


    $edges = $DB->get_records('conceptmaps_edges', ["auto_correction" => 17],'', 'id');
    return $this->myrenderer->render_single_correction(new singleCorrection($this->cmid, $topic));
  }


  private function handle_solution($withCorrection) {
    global $CFG, $DB, $PAGE;
    $submission = optional_param('submission', null, PARAM_INT);
    if($submission == null) {
      $topicid = required_param('topic', PARAM_INT);
      //get first submission of this topic
      $submission_obj = array_values($DB->get_records('conceptmaps_submissions', ['conceptmapstopic' => $topicid], "version DESC", "*", 0, 1))[0];

      $userid = $submission_obj->userid;
      $submission = $submission_obj->id;
    } else {
      $submission_obj = $DB->get_record('conceptmaps_submissions', ['id' => $submission]);
      $topicid = $submission_obj->conceptmapstopic;
      $userid = $submission_obj->userid;
    }



    return $this->myrenderer->render_conceptmap(prepare_conceptmap($this->cmid, $this->conceptmapsid, $topicid, $userid, $submission, $withCorrection));
  }

  private function handle_autocorrection() {
    global $CFG;
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/autocorrection.php');
    return $this->myrenderer->render_autocorrection(new autocorrection($this->cmid, $this->conceptmapsid));
  }

  private function handle_correct_conceptmap() {
    global $DB;
    $submissionid = required_param('submission', PARAM_INT);
    $update = new stdClass();
    // if the user changed the comment should be saved
    $comment = optional_param('comment', null, PARAM_TEXT);
    if($comment != null) {
      $comment = trim($comment);
      $comment = stripslashes($comment);
      $comment = htmlspecialchars($comment);
      //If there is a comment it should be saved
      $update->feedback = $comment;
    }
    $corrected = optional_param('corrected', 0, PARAM_BOOL);
    $update->corrected = $corrected;
    $failed = optional_param('redo', 0, PARAM_BOOL);
    $update->failed = $failed;

    $update->id = $submissionid;
    $DB->update_record("conceptmaps_submissions", $update);


    $redirectionTarget = new moodle_url('/mod/conceptmaps/view.php', array('id'=>$this->cmid, 'submission'=>$submissionid, 'action'=>'solution'));
    redirect($redirectionTarget->out());
  }
}
