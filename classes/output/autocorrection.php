<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * The purpose of this script is to collect the output data for the template and
 * make it available to the renderer.
 */
class Autocorrection implements \renderable, \templatable {

    private $cmid;
    private $conceptmapsid;
    private $edges;
    private $perpage;

    /**
     * Constructor of renderable for autocorrection tab.
     * @param int $conceptmapsid Id of the conceptmaps instance
     */
    public function __construct($cmid, $conceptmapsid) {
      $this->cmid = $cmid;
      $this->conceptmapsid = $conceptmapsid;
      $this->perpage = 5;
    }

    private function get_table() {
      global $OUTPUT, $PAGE, $DB;
      $ret = "";
      $page = optional_param('page', 0, PARAM_INT);
      $this->perpage = optional_param('perpage', $this->perpage, PARAM_INT);
      $url = new moodle_url('/mod/conceptmaps/view.php', array('id' => $this->cmid, "action" => Action::get_string(Action::Autocorrection), "page" => $page, "perpage" => $this->perpage));

      $sql = "SELECT count(id) as 'count' FROM mdl_conceptmaps_auto_edges WHERE conceptmapstopic IN (SELECT id FROM mdl_conceptmaps_topics WHERE conceptmapsid = 2)";
      $count = $DB->get_record_sql($sql, ["conceptmapsid" => $this->conceptmapsid])->count;


      $pagingbar = $OUTPUT->paging_bar($count, $page, $this->perpage, $url);
      $start = $page * $this->perpage;
      $sql2 = "SELECT * FROM mdl_conceptmaps_auto_edges WHERE conceptmapstopic IN (SELECT id FROM mdl_conceptmaps_topics WHERE conceptmapsid = :conceptmapsid)";

      $this->edges = $DB->get_records_sql($sql2." ORDER BY id ASC", ['conceptmapsid' => $this->conceptmapsid], $start, $this->perpage);

      $html = '';

      $fulltable = new html_table();
      $fulltable->attributes['class'] = 'table table-striped ';
      $fulltable->attributes['class'] .= 'single-correction-table ';
      $fulltable->id = 'auto-correction';
      $fulltable->summary = "Summary";
      $fulltable->head = [get_string('source', 'conceptmaps'), get_string('content', 'conceptmaps'), get_string('target', 'conceptmaps'), get_string('status', 'conceptmaps'), get_string('comment', 'conceptmaps'), get_string('corrected', 'conceptmaps'), get_string('delete', 'conceptmaps')];
      $fulltable->data = $this->get_rows();
      $html .= html_writer::table($fulltable);

      $ret = $pagingbar;
      $ret .= $OUTPUT->container($html, 'correctionparent');

      return $ret;
    }

    private function get_rows() {
      $rows = [];

      // Body
      foreach ($this->edges as $key => $edge) {

        $row = new html_table_row();
        $row->id = 'edge_'.$edge->id;

        // cell name
        $source_cell = new html_table_cell();
        $source_cell->text = $edge->source;
        $row->cells[] = $source_cell;

        $content_cell = new html_table_cell();
        $content_cell->text = $edge->content;
        $row->cells[] = $content_cell;

        $target_cell = new html_table_cell();
        $target_cell->text = $edge->target;
        $row->cells[] = $target_cell;

        $autocorrection_cell = new html_table_cell();

        $status_cell = new html_table_cell();
        $status_cell->text = $this->get_status_cell($edge);
        $row->cells[] = $status_cell;

        $comment_cell = new html_table_cell();
        $comment_cell->text = html_writer::tag('input', null, array('class'=>'comment', 'id' => 'comment_'.$edge->id, 'data-cmid'=>$this->cmid, 'value' => $edge->comment));
        $row->cells[] = $comment_cell;

        $corrected_cell = new html_table_cell();
        $corrected_cell->text = $this->get_verified_cell($edge);
        $row->cells[] = $corrected_cell;

        $delete_cell = new html_table_cell();
        $delete_cell->text = $this->get_delete_cell($edge);
        $row->cells[] = $delete_cell;

        $rows[] = $row;
      }

      return $rows;
    }

    private function get_verified_cell($edge) {

      $o = "";
      $o .= html_writer::start_tag('form', array('action' => "#", 'method' => 'post'));
      $o .= html_writer::tag('button', get_string('check', 'conceptmaps'), array('id' => 'verified_'.$edge->id, 'data-cmid'=>$this->cmid, 'disabled' => true ));
      $o .= html_writer::end_tag('form');
      return $o;
    }

    private function get_delete_cell($edge) {

      $o = "";
      $o .= html_writer::tag('button', get_string('delete', 'conceptmaps'), array('id' => 'autoedge_'.$edge->id, 'data-cmid'=>$this->cmid ));
      return $o;
    }

    private function get_status_cell($edge) {
      if($edge->verification >= 2) {
        $formal_selected = true;
      } else {
        $formal_selected = false;
      }
      $formal_id = 'formal_correct_'.$edge->id;
      $formal_correct = html_writer::checkbox($formal_id, 2, $formal_selected, null, array('id'=>$formal_id, 'data-cmid' => $this->cmid));

      if($edge->verification == 1 || $edge->verification == 3) {
        $content_selected = true;
      } else {
        $content_selected = false;
      }

      $content_id =  'content_correct_'.$edge->id;
      $content_correct = html_writer::checkbox($content_id, 2, $content_selected, null, array('id'=>$content_id, 'data-cmid' => $this->cmid ));

      return $formal_correct.$content_correct;
    }

    /**
     * This function is required by any renderer to retrieve the data structure
     * passed into the template.
     * @param \renderer_base $output
     * @return type
     */
    public function export_for_template(\renderer_base $output) {

        $data = new stdClass();
        $data->table = $this->get_table();

        return $data;
    }


}
