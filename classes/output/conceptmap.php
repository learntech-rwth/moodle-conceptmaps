<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/mod/conceptmaps/locallib.php');
require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/term.php');
/**
 * Class to prepare conceptmap for display.
 *
 * @package   mod_conceptmaps
 */
class conceptmap implements templatable, renderable {
  public $id;
  public $cmid;
  public $conceptmapsid;
  public $topicid;
  public $topic;
  public $version;
  public $edges;
  public $doCorrection;
  public $comment;
  public $switchUsers = false;
  public $options = array();
  public $versions = array();
  public $failed = false;
  public $corrected = false;
  public $corrector = false;

  /**
   * Constructor.
   *
   * @param \stdClass $result
   */
  public function __construct($cmid, $conceptmapsid, $topicid, $id, $doCorrection) {
    global $DB, $USER;
      $this->cmid = $cmid;
      $this->conceptmapsid = $conceptmapsid;
      $this->topicid = $topicid;
      $this->doCorrection = $doCorrection;

      if(isset($id)) {
        // get data
        $this->id = $id;
        $this->new = false;
        $submission_data= $DB->get_record('conceptmaps_submissions', ['id' => $this->id]);
        $userid = $submission_data->userid;
        $this->comment = $submission_data->feedback;
        $this->corrected = $submission_data->corrected;
        $this->failed = $submission_data->failed;
        $this->version = $submission_data->version;
        $this->topic = get_topic_user($this->topicid, $userid, $this->version);

        $this->edges = get_edges_user($this->topicid, $userid, $this->version);


        if($doCorrection) {
          $context = context_module::instance($this->cmid);
          //selection of users
          if(has_capability('mod/conceptmaps:editsettings', $context)){
            $this->switchUsers = true;
            $this->corrector = true;
            //then get options (the different users with submissons in this topic)
            // TODO: it would be better, that the version would be the highest instead of any (to get the newest version by default)
            $users = $DB->get_records_sql("SELECT u.*, s.id as submissionid FROM mdl_user as u JOIN mdl_conceptmaps_submissions s ON u.id = s.userid WHERE s.conceptmapstopic = :topic AND s.submitted = 1 GROUP BY u.id", ["topic" => $this->topicid]);

            foreach ($users as $key => $user) {
              $selected = false;
              if($user->id == $userid) $selected = true;
              $this->options[] = ["name" => fullname($user), "value" => $user->submissionid, "selected" => $selected];
            }
          }

          //selection of version
          $versions = $DB->get_records("conceptmaps_submissions", ["userid" => $userid, "conceptmapstopic" => $this->topicid, "submitted" => 1], "version ASC");

          foreach ($versions as $key => $value) {
            $selected = false;
            if($value->version == $this->version) $selected = true;
            $this->versions[] = ["name" => "Version ".$value->version, "value" => $value->id, "selected" => $selected];
          }
        }

      } else {
        $context = context_module::instance($this->cmid);
        if(!has_capability('mod/conceptmaps:onlystudent', $context)){
            $redirectionTarget = new moodle_url('/login/index.php', array('id'=>$this->cmid));
            redirect($redirectionTarget->out());
        }
        $this->version = $DB->count_records("conceptmaps_submissions", ["conceptmapstopic" => $this->topicid, "userid" => $USER->id]) + 1;
        // create new conceptmap for the user
        // create new instance in DB
        $this->edges = array();
        $this->createSubmission($this->version, $this->version > 1);

        $this->new = true;


      }
  }

  private function createSubmission($version, $copy) {
    global $DB, $USER;
    $topic = get_topic($this->topicid);

    try{
      $transaction = $DB->start_delegated_transaction();
      // insert submission instance
      $dataobject = new stdClass();
      $dataobject->conceptmapstopic = $this->topicid;
      $dataobject->userid = $USER->id;
      $dataobject->timecreated = time();
      $dataobject->timemodified = time();
      $dataobject->corrected = false;
      $dataobject->feedback = null;
      $dataobject->failed = 0;
      $dataobject->version = $version;
      $id = $DB->insert_record("conceptmaps_submissions", $dataobject, true, $bulk=false);
      // set id
      $this->id = $id;
      $this->topic = $topic;

      // copy from old version instead of whole new
      if($copy) {
        $originalterms = array_values($DB->get_records("conceptmaps_student_terms", ["conceptmapstopic" => $this->topicid, "userid" => $USER->id, "version" => ($this->version - 1)]));
        $this->topic->terms = [];
        // insert Terms in student terms table
        foreach ($originalterms as $key => $value) {
          $term = new stdClass();
          $term->name = $value->name;
          $term->conceptmapstopic = $value->conceptmapstopic;
          $term->editable = $value->editable;
          $term->positionx = $value->positionx;
          $term->positiony = $value->positiony;
          $term->originalterm = $value->originalterm;
          $term->userid = $USER->id;
          $term->version = $version;

          $id = $DB->insert_record("conceptmaps_student_terms", $term, true, $bulk=false);
          $term->id = $id;
          $this->topic->terms[] = $term;
        }

        $edges = $DB->get_records("conceptmaps_edges", ["conceptmapstopic" => $this->topicid, "userid" => $USER->id, "version" => ($this->version - 1)]);
        foreach ($edges as $key => $edge) {
          $edge->version = $this->version;
          $edge->source = $this->get_new_termid($originalterms, $edge->source);
          $edge->target = $this->get_new_termid($originalterms, $edge->target);
          $DB->insert_record("conceptmaps_edges", $edge);
          $this->edges[] = $edge;
        }
      } else {
        $originalterms = $topic->terms;
        $this->topic->terms = [];
        // insert Terms in student terms table
        foreach ($originalterms as $key => $value) {
          $term = new stdClass();
          $term->name = $value->name;
          $term->conceptmapstopic = $value->conceptmapstopic;
          $term->editable = $value->editable;
          $term->positionx = $value->positionx;
          $term->positiony = $value->positiony;
          $term->originalterm = $value->id;
          $term->userid = $USER->id;
          $term->version = $version;

          $id = $DB->insert_record("conceptmaps_student_terms", $term, true, $bulk=false);
          $term->id = $id;
          $this->topic->terms[] = $term;
        }
      }

      $transaction->allow_commit();
    }
    catch(Exception $e) {
        $this->uploadSuccessful = false;
        $transaction->rollback($e);
    }
  }

  private function get_new_termid($originalterms, $oldid) {

    foreach ($originalterms as $key => $oldterm) {
      if($oldterm->id == $oldid) {
        $id = $oldterm->originalterm;
        break;
      }
    }
      foreach ($this->topic->terms as $key => $term) {
        if($term->originalterm == $id) {
          return $term->id;
        }
      }
      return null;
  }

  /**
   * Function to export the renderer data in a format that is suitable for a mustache template.
   *
   * @param \renderer_base $output Used to do a final render of any components that need to be rendered for export.
   * @return \stdClass|array
   */
  public function export_for_template(\renderer_base $output) {
      $result = new \stdClass();
      $result->cmid = $this->cmid;
      $result->conceptmapsid = $this->conceptmapsid;
      $result->topicid = $this->topicid;
      $result->topic_name = $this->topic->name;
      $result->submissionid = $this->id;
      $result->newVersion = $this->new;
      foreach ($this->topic->terms as $key => $term) {
        $term->cmid = $this->cmid;
        $term = new term($term);
        $result->terms[] = $term->export_for_template($output); //(new \core\output\inplace_editable('mod_conceptmaps', 'conceptmaps_term', $term->id, $term->editable, format_string($term->name), $term->name, 'Edit',  'New value for ' . format_string($term->name)))->export_for_template($output);
      }
      $result->doCorrection = $this->doCorrection;
      $result->comment = $this->comment;
      $result->corrected = $this->corrected;
      $result->redo = $this->failed;
      $result->switchUsers = $this->switchUsers;
      $result->options = $this->options;
      $result->versions = $this->versions;
      $result->corrector = $this->corrector;

      return $result;
  }
}
