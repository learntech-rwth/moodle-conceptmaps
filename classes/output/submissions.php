<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/topic.php');

/**
 * The purpose of this script is to collect the output data for the template and
 * make it available to the renderer.
 */
class submissions implements \renderable, \templatable {

    private $cmid;
    private $conceptmapsid;
    private $topics;
    private $topicsForSubmissions;

    /**
     * Constructor of renderable for submissions tab.
     * @param int $conceptmapsid Id of the conceptmaps instance
     */
    public function __construct($cmid, $conceptmapsid) {
      global $DB;
      $this->cmid = $cmid;
      $this->conceptmapsid = $conceptmapsid;
      $this->topicsForSubmissions = [];
      // Get all topics
      $topics = $DB->get_records('conceptmaps_topics', ['conceptmapsid' => $this->conceptmapsid]);
      $this->topics = $topics;
      $this->prepareTopicsForSubmissions();
    }

    private function prepareTopicsForSubmissions () {
      global $DB;
      foreach ($this->topics as $key => $topic) {
        $topic->cmid = $this->cmid;
        $topic = new topic($topic);

        $topicsubmission = new stdClass();
        $topicsubmission->status = $topic->getStatus();
        $topicsubmission->topicid = $topic->id;
        $topicsubmission->topicname = $topic->name;

        $topicsubmission->count = $DB->count_records("conceptmaps_submissions", ["conceptmapstopic"=>$topic->id, "version" => 1, "submitted" => 1]);
        $topicsubmission->empty = $topicsubmission->count == 0 ? true : false;
        $topicsubmission->correctionAllowed = $this->isCorrectionAllowed($topic);
        $this->topicsForSubmissions[] = $topicsubmission;
      }
    }

    private function isCorrectionAllowed($topic) {
      global $DB;
      if($topic->isActiveCorrection()) {
        return true;
      } else if($topic->isActiveCorrection() || $topic->isOverCorrection() || $topic->isWithoutStartcorrection()) {
        $count = $DB->count_records("conceptmaps_submissions", ["conceptmapstopic"=>$topic->id, "corrected" => 0, "submitted" => 1]);
        if($count > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    /**
     * This function is required by any renderer to retrieve the data structure
     * passed into the template.
     * @param \renderer_base $output
     * @return type
     */
    public function export_for_template(\renderer_base $output) {
        $data = new stdClass();
        $data->topicsForSubmissions = $this->topicsForSubmissions;
        $data->cmid = $this->cmid;
        return $data;
    }

}
