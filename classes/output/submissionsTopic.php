<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/conceptmaps/locallib.php');
/**
 * The purpose of this script is to collect the output data for the template and
 * make it available to the renderer.
 */
class submissionsTopic implements \renderable, \templatable {

    private $cmid;
    private $topicid;
    private $submissions;

    /**
     * Constructor of renderable for submissions tab.
     * @param int $conceptmapsid Id of the conceptmaps instance
     */
    public function __construct($cmid, $topicid) {
      global $DB;
      $this->cmid = $cmid;
      $this->topicid = $topicid;
      // Get all topics
      $submissions = $DB->get_records_sql('SELECT max(id) as "id", userid, max(version) FROM {conceptmaps_submissions} WHERE conceptmapstopic = :topic Group by userid', ['topic'=>$topicid]);
      $this->prepareSubmissions($submissions);
    }

    private function prepareSubmissions ($submissions) {
      foreach ($submissions as $key => $submission) {
        $topicsubmission = new stdClass();
        $topicsubmission->color = "color: green;"; //TODO
        $topicsubmission->name = conceptmap_get_username($submission->userid);
        $topicsubmission->id = $submission->id;
        $this->submissions[] = $topicsubmission;
      }
    }

    /**
     * This function is required by any renderer to retrieve the data structure
     * passed into the template.
     * @param \renderer_base $output
     * @return type
     */
    public function export_for_template(\renderer_base $output) {
        $data = new stdClass();
        $data->submissions = $this->submissions;
        $data->cmid = $this->cmid;
        $data->topicid = $this->topicid;

        return $data;
    }

}
