<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Class to prepare verification results for display.
 *
 * @package   mod_conceptmaps
 * @copyright 2017 Mark Nelson <markn@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class topic implements templatable, renderable {
  public $id;
  public $cmid;
  public $conceptmapsid;
  public $name;
  public $start;
  public $end;
  public $startcorrection;
  public $endcorrection;
  public $corrected;
  public $mform;
  public $terms;
  public $redo;
  public $submission;

  /**
   * Constructor.
   *
   * @param \stdClass $result
   */
  public function __construct($result) {
    global $DB, $USER;
      $this->cmid = $result->cmid;
      $this->id = $result->id;
      $this->conceptmapsid = $result->conceptmapsid;
      $this->name = $result->name;
      $this->start = $result->start;
      $this->end = $result->end;
      $this->startcorrection = $result->startcorrection;
      $this->endcorrection  = $result->endcorrection;

      if(isset($result->mform)) {
        $this->mform = $result->mform;
      }
      if(isset($result->terms)) {
        $this->terms = array_values($result->terms);
      }

      $context = context_module::instance($this->cmid);

      if(has_capability('mod/conceptmaps:onlystudent', $context)){
        // get newest conceptmap
        $submission_objs = array_values($DB->get_records('conceptmaps_submissions', ['conceptmapstopic' => $this->id, 'userid' => $USER->id], "version DESC", "*"));

        if($submission_objs != null && count($submission_objs) > 0) {
          $submission_obj = $submission_objs[0]; // get current(newest) submission
          if(($submission_obj->failed && $submission_obj->submitted && $submission_obj->corrected) || (!$submission_obj->submitted && count($submission_objs) > 1)) {
            // If the lastest conceptmap was submitted but failed or if the latest one is not yet submitted the user should be able to edit it
            $this->redo = true;
          }
          $this->submission = true;
          $this->submitted = $submission_obj->submitted;
          $this->corrected = $submission_obj->corrected;
        } else {
          $this->submission = false;
          $this->submitted = false;
        }
      }
  }


  /**
   * Hier wird überprüft, ob das Thema in diesem Moment aktiv ist.
   * Also ob das Startdatum in der Vergangenheit und das Enddatum in der Zukunft liegt.
   * Vorher wird überprüft, ob es überhaupt Eckdaten gibt.
   * @return boolean
   */
  public function isActive(){
      $startdate = $this->start;
      $enddate = $this->end;
      $now = time();
      if($this->isWithoutStart()&& $this->isWithoutEnd()){
          return true;
      }else if($this->isWithoutEnd()){
          if($startdate<$now){
              return true;
          }
      }else if($this->isWithoutStart()){
          if($enddate>$now){
              return true;
          }
      }else if($startdate<$now){
          if($enddate>$now){
              return true;
          }
      }
      return false;
  }

  private function isWithoutStart() {
    return $this->start == null || $this->start == "" || $this->start == 0;
  }

  private function isWithoutEnd() {
    return $this->end == null || $this->end == "" || $this->end == 0;
  }

  /**
   * Hier wird überprüft, ob das Thema bevorstehend ist.
   * Also wenn Das Startdatum in der Zukunft liegt. Liegt kein Startdatum vor,
   * ist es auch nicht bevorstehend, da es dann entweder aktiv ist
   * oder schon beendet, wenn das Enddatum in der Vergangenheit liegt.
   * @return boolean
   */
  public function isUpcoming(){
      $startdate = $this->start;
      $enddate = $this->end;
      $now = time();
      if($this->isWithoutStart()){
          return false;
      }
      if($startdate>$now){
          return true;
      }
      return false;
  }

  /**
   * Hier wird überprüft, ob das Thema schon vorbei ist.
   * Dies ist nur der Fall, wenn es ein Enddatum gibt,
   *  das in der Vergangenheit liegt.
   * @return boolean
   */
  public function isOver(){
      if($this->isWithoutEnd()){
          return false;
      }
      $enddate = $this->end;
      $now = time();
      if($enddate<$now){
          return true;
      }
      return false;
  }

      /**
   * Hier wird überprüft, ob das Thema in diesem Moment aktiv ist.
   * Also ob das Startdatum in der Vergangenheit und das Enddatum in der Zukunft liegt.
   * Vorher wird überprüft, ob es überhaupt Eckdaten gibt.
   * @return boolean
   */
  public function isActiveCorrection(){
      $startdate = $this->startcorrection;
      $enddate = $this->endcorrection;
      $now = time();
      if($startdate<$now){
          if($enddate>$now){
              return true;
          }
      }
      return false;
  }

  /**
   * Hier wird überprüft, ob das Thema bevorstehend ist.
   * Also wenn Das Startdatum in der Zukunft liegt. Liegt kein Startdatum vor,
   * ist es auch nicht bevorstehend, da es dann entweder aktiv ist
   * oder schon beendet, wenn das Enddatum in der Vergangenheit liegt.
   * @return boolean
   */
  public function isUpcomingCorrection(){
      $startdate = $this->startcorrection;
      $enddate = $this->endcorrection;
      $now = time();
      if($this->isWithoutStartcorrection()){
          return false;
      }
      if($startdate>$now){
          return true;
      }
      return false;
  }

  public function isWithoutStartcorrection() {
    return $this->startcorrection == null || $this->startcorrection == "" || $this->startcorrection == 0;
  }

  public function isWithoutEndcorrection() {
    return $this->endcorrection == null || $this->endcorrection == "" || $this->endcorrection == 0;
  }

  /**
   * Hier wird überprüft, ob das Thema schon vorbei ist.
   * Dies ist nur der Fall, wenn es ein Enddatum gibt,
   *  das in der Vergangenheit liegt.
   * @return boolean
   */
  public function isOverCorrection(){
      if($this->isWithoutEndcorrection()){
          return false;
      }
      $enddate = $this->endcorrection;
      $now = time();
      if($enddate<$now){
          return true;
      }
      return false;
  }

  public function getStatus(){

    // student or teacher
    $context = context_module::instance($this->cmid);
    if(has_capability('mod/conceptmaps:editsettings', $context)) {
      // teacher
      return $this->getStatusTeacher();
    } else {
      return $this->getStatusStudent();
    }
  }

  private function getStatusTeacher() {
    global $DB;
    if($this->isUpcoming()){
        return get_string('status_upcoming', 'conceptmaps');
    }elseif($this->isActive()){
        return get_string('status_active', 'conceptmaps');;
    }elseif($this->isOver() && $this->isUpcomingCorrection()){
        return get_string('status_corr_upcoming', 'conceptmaps');;
    }else{
      //get highest version
      $highestVersion = $DB->get_record_sql('SELECT max(version) as version FROM mdl_conceptmaps_submissions WHERE conceptmapstopic = :topic', ["topic" => $this->id])->version;
      if($highestVersion == null) {
        return get_string('status_nothing_submitted', 'conceptmaps');;
      }
      for($i=1; $i <= $highestVersion; $i++) {
        $submissionsOfVersionI = $DB->get_records('conceptmaps_submissions', ["conceptmapstopic" => $this->id, "version" => $i]);
        $count = count($submissionsOfVersionI);
        $submittedButNotCorrected = 0;
        $failed = 0;
        $notSubmitted = 0;
        foreach ($submissionsOfVersionI as $key => $submission) {
          if($submission->submitted == 0) {
            $notSubmitted++;
          } else if($submission->corrected == 0) {
            $submittedButNotCorrected++;
          }else if($submission->failed == 1) {
            $failed++;
          }
        }
        if($notSubmitted == $count) {
          return get_string('status_wait_submission', 'conceptmaps');
        }
        if($submittedButNotCorrected > 0) {
          return get_string('status_i_corr_active', 'conceptmaps', $i);
        }
        if($failed == 0) {
          return get_string('status_completed', 'conceptmaps');
        }
        if($failed > 0) {
          return get_string('status_wait_submission', 'conceptmaps');
        }
      }
      return get_string('status_undefined', 'conceptmaps');
    }
  }

  private function getStatusStudent() {
    global $USER, $DB;
    if($this->isUpcoming()){
        return get_string('status_upcoming', 'conceptmaps');
    }elseif(($this->isOver() || $this->submitted) && $this->isUpcomingCorrection()){
        return get_string('status_corr_upcoming', 'conceptmaps');
    }elseif($this->isActiveCorrection()){
        return get_string('status_corr_active', 'conceptmaps');
    }else{
        // differentiate between failed and passed and nothing submitted
        $submissions = $DB->get_records("conceptmaps_submissions", ["conceptmapstopic" => $this->id, "userid" => $USER->id]);

        if($submissions == null || count($submissions) == 0) {
          if($this->isActive()) {
            return get_string('status_active', 'conceptmaps');
          } else {
            return get_string('status_nothing_submitted', 'conceptmaps');
          }
        } else if(count($submissions) >= 1) {
          // look for newest version
          $version = 0;
          $submission = null;
          foreach ($submissions as $key => $value) {
            if($value->version > $version) {
              $submission = $value;
            }
          }
          if($submission != null ) {
            if(!$submission->submitted || $submission->failed) {
              if(count($submissions) == 1 && (!$submission->failed || !$submission->submitted)) {
                // if it is the first submission it should not be resubmit, but only active
                return get_string('status_active', 'conceptmaps');
              }
              return get_string('status_resubmit', 'conceptmaps');
            } else {
              if($submission->feedback == null || $submission->feedback == "") {
                return get_string('status_corr_active', 'conceptmaps');
              } else {
                return get_string('status_completed', 'conceptmaps');
              }
            }
          }
        }
        return get_string('status_undefined', 'conceptmaps');
    }
  }

  /**
   * Function to export the renderer data in a format that is suitable for a mustache template.
   *
   * @param \renderer_base $output Used to do a final render of any components that need to be rendered for export.
   * @return \stdClass|array
   */
  public function export_for_template(\renderer_base $output) {
      $result = new \stdClass();
      $result->cmid = $this->cmid;
      $result->conceptmapsid = $this->conceptmapsid;
      $result->topicid = $this->id;
      $result->topicname = $this->name;
      $result->start = $this->start;
      $result->end = $this->end;
      $result->startcorrection = $this->startcorrection;
      $result->endcorrection  = $this->endcorrection;
      $result->corrected = $this->corrected;
      $result->status = $this->getStatus(); //TODO: wenn in DB richtiges DAtum ist, dann hier status napassen. Momentan wirft es noch einen Fehler
      $result->edittopicform = $this->mform;
      $result->terms = $this->terms;
      $result->redo = $this->redo;
      $result->submission = $this->submission;
      $result->submitted = $this->submitted;

      return $result;
  }
}
