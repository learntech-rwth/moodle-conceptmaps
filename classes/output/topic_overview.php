<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
//namespace mod_conceptmaps\output;
require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/topic.php');
/**
 * The purpose of this script is to collect the output data for the template and
 * make it available to the renderer.
 */
class topic_overview implements \renderable, \templatable {

    public $cmid;
    public $conceptmapsid;
    public $topics;

    /**
     * Constructor of renderable for topic_overview tab.
     * @param int $conceptmapsid Id of the conceptmaps instance
     */
    public function __construct($cmid, $conceptmapsid) {
      global $DB, $USER;
      $this->cmid = $cmid;
      $this->conceptmapsid = $conceptmapsid;

      $topics = $DB->get_records('conceptmaps_topics', ['conceptmapsid' => $this->conceptmapsid], $sort='', $fields='*', $limitfrom=0, $limitnum=0);
      $this->topics = $topics;



    }


    /**
     * This function is required by any renderer to retrieve the data structure
     * passed into the template.
     * @param \renderer_base $output
     * @return type
     */
    public function export_for_template(\renderer_base $output) {

      $result = new \stdClass();
      $result->cmid = $this->cmid;
      $result->conceptmapsid = $this->conceptmapsid;
      $result->topics = array();
      $result->activeTopics = array();
      $result->upcomingTopics = array();
      $result->completedTopics = array();
      $result->action_createtopic = Action::get_string(Action::CreateTopic);
      foreach ($this->topics as $topic) {
        $topic->cmid = $this->cmid;
        $resulttopic = new topic($topic);

        if($resulttopic->isActive()) {
          if($resulttopic->isWithoutStartcorrection() && $resulttopic->submitted) {
            $result->completedTopics[] = $resulttopic->export_for_template($output);
          } else {
            $result->activeTopics[] = $resulttopic->export_for_template($output);
          }
        } else if ($resulttopic->isUpcoming()) {
          $result->upcomingTopics[] = $resulttopic->export_for_template($output);
        } else {
          $result->completedTopics[] = $resulttopic->export_for_template($output);
        }
      }
      if(count($this->topics) == 0) {
        $result->noTopics = true;
      } else {
        $result->noTopics = false;
      }
      return $result;
    }

}
