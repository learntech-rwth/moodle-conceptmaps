<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');
require_once(__DIR__.'/locallib.php');


// Course_module ID, or
$id = optional_param('id', 0, PARAM_INT);

// ... module instance id.
$l  = optional_param('l', 0, PARAM_INT);

if ($id) {
    $cm             = get_coursemodule_from_id('conceptmaps', $id, 0, false, MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('conceptmaps', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($l) {
    $moduleinstance = $DB->get_record('conceptmaps', array('id' => $n), '*', MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm             = get_coursemodule_from_instance('conceptmaps', $moduleinstance->id, $course->id, false, MUST_EXIST);
} else {
    print_error(get_string('missingidandcmid', 'mod_conceptmaps'));
}

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);

$topicid = required_param('topicid', PARAM_INT);
$conceptmapid = required_param('conceptmapid', PARAM_INT);
$finish = required_param('finish', PARAM_BOOL);
$edges = required_param('edges', PARAM_TEXT);
$edges_to_delete = required_param('edges_to_delete', PARAM_TEXT);
$nodes = required_param('nodes', PARAM_TEXT);
$edited = required_param('edited', PARAM_BOOL);
$submitted = required_param('submitted', PARAM_INT);
$version = required_param('version', PARAM_INT);

$nodes = json_decode($nodes);
$edges = json_decode($edges);
$edges_to_delete = json_decode($edges_to_delete);

try {
  $transaction = $DB->start_delegated_transaction();
  // Update conceptmap instance
  $conceptmap = new stdClass();
  $conceptmap->id = $conceptmapid;
  $conceptmap->timemodified = time();
  $conceptmap->submitted = $submitted;
  $DB->update_record("conceptmaps_submissions", $conceptmap);

  // Update nodes
  foreach ($nodes as $key => $node) {
    $DB->update_record("conceptmaps_student_terms", $node);
  }

  // Update, delete and add edges
  //delete the ones to delete
  foreach ($edges_to_delete as $key => $edge) {
    $DB->delete_records("conceptmaps_edges", ["conceptmapstopic" => $topicid, "userid" => $USER->id, "version" => $version, "connectionid" => $edge->connectionid]);
  }



  // update existing once and insert new ones
  foreach ($edges as $key => $edge) {

    $record = $DB->get_record("conceptmaps_edges", ["conceptmapstopic" => $topicid, "userid" => $USER->id, "version" => $version, "connectionid" => $edge->connectionid]);
    if($record != null) {
      $data = new stdClass();
      $data->id = $record->id;
      $data->conceptmapstopic = $topicid;
      $data->source = $edge->sourceid;
      $data->target = $edge->targetid;
      $data->content = $edge->content;
      $data->version = $version;
      $data->connectionid = $edge->connectionid;
      if($record->source != $edge->sourceid || $record->target != $edge->targetid || $record->content != $edge->content || $record->connectionid != $edge->connectionid) {
        // Unset correction since edge has changed
        $data->verification = 0;
        $data->verified = 0;
        $data->comment = 0;
        $data->auto_correction = null;
      }
      $DB->update_record("conceptmaps_edges", $data);
    } else {
      $data = new stdClass();
      $data->conceptmapstopic = $topicid;
      $data->source = $edge->sourceid;
      $data->target = $edge->targetid;
      $data->userid = $USER->id;
      $data->content = $edge->content;
      $data->verification = 0;
      $data->verified = 0;
      $data->version = $version;
      $data->connectionid = $edge->connectionid;

      $DB->insert_record("conceptmaps_edges", $data);
    }
  }

  $transaction->allow_commit();
  echo json_encode(['status' => 'success']);
} catch(Exception $e) {
  $transaction->rollback($e);
  echo json_encode(['status' => 'error', 'log' => get_string('error:missingAnnotationtype', 'pdfannotator'), 'err' => $e->getMessage()]);
}
