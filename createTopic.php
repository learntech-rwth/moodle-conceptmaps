<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');
require_once(__DIR__.'/locallib.php');

require_once(__DIR__.'/classes/controller/create_topic_controller.php');
require_once(__DIR__.'/classes/forms/create_topic_form.php');


// Course_module ID, or
$id = optional_param('id', 0, PARAM_INT);

// ... module instance id.
$l  = optional_param('l', 0, PARAM_INT);

if ($id) {
    $cm             = get_coursemodule_from_id('conceptmaps', $id, 0, false, MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('conceptmaps', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($l) {
    $moduleinstance = $DB->get_record('conceptmaps', array('id' => $n), '*', MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm             = get_coursemodule_from_instance('conceptmaps', $moduleinstance->id, $course->id, false, MUST_EXIST);
} else {
    print_error(get_string('missingidandcmid', 'mod_conceptmaps'));
}

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);

$PAGE->set_url('/mod/conceptmaps/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($moduleinstance->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);



$controller = new mod_conceptmaps_create_topic_controller($cm->id, $moduleinstance->id);

$controller->handle_access();

$cancel = optional_param('cancel', false, PARAM_BOOL);
if($cancel){
    $controller->handle_cancel();
}





$topicid = optional_param('topic', 0, PARAM_INT);

if($topicid) {


  $topic = $DB->get_record('conceptmaps_topics', ["id" => $topicid]);

  $terms = $DB->get_records('conceptmaps_terms', ["conceptmapstopic" => $topicid]);
  $count_empty_terms = $DB->count_records('conceptmaps_terms', array('conceptmapstopic'=>$topicid, 'name' => ""));
  $terms = get_names_of_terms($terms);

  $terms_count = count($terms) - $count_empty_terms;

  $mform = new mod_conceptmaps_create_topic_form(null, array('topicid' => $topicid, 'termcount' => $terms_count, 'submit_string' => get_string('topic_edit_submit', 'conceptmaps')));
  $data = array('cmid'=> $cm->id, 'id'=> $cm->id, 'name' => $topic->name, 'start' => $topic->start, 'end' => $topic->end, 'startcorrection' => $topic->startcorrection, 'endcorrection' => $topic->endcorrection, 'term' => $terms, 'count_empty_terms' => $count_empty_terms);

  $mform->set_data($data);

  $controller->handle_form($mform, false);
} else {
  $mform = new mod_conceptmaps_create_topic_form(null, array('termcount' => 2, 'submit_string' => get_string('topic_create_submit', 'conceptmaps')));
  $mform->set_data(array('cmid'=> $cm->id, 'id'=> $cm->id));

  $controller->handle_form($mform, true);
}

echo $OUTPUT->header();

$mform->display();

echo $OUTPUT->footer();
