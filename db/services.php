<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 defined('MOODLE_INTERNAL') || die();

 $services = array(
       'conceptmaps_services' => array(                                                //the name of the web service
           'functions' => array ('mod_conceptmaps_add_auto_edge'), //web service functions of this service
           'requiredcapability' => 'mod/conceptmaps:editsettings',                //if set, the web service user need this capability to access
                                                                               //any function of this service. For example: 'some/capability:specified'
           'restrictedusers' =>0,                                             //if enabled, the Moodle administrator must link some user to this service
                                                                               //into the administration
           'enabled'=>1,                                                       //if enabled, the service can be reachable on a default installation
        )
   );

$functions = array(
    'mod_conceptmaps_add_auto_edge' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'add_auto_edge',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Adds an edge to the auto edges',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
    'mod_conceptmaps_remove_auto_edge' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'remove_auto_edge',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Removes an edge from the auto edges',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
    'mod_conceptmaps_change_verification' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'change_verification',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Changes the verification of an edge',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
    'mod_conceptmaps_change_verification_autoedge' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'change_verification_autoedge',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Changes the verification of an auto edge',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
    'mod_conceptmaps_change_comment' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'change_comment',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Changes the verification of an edge',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
    'mod_conceptmaps_change_comment_autoedge' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'change_comment_autoedge',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Changes the verification of an autoedge',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
    'mod_conceptmaps_change_verified' => array(
        'classname'   => 'mod_conceptmaps_external',
        'methodname'  => 'change_verified',
        'classpath'   => 'mod/conceptmaps/externallib.php',
        'description' => 'Changes the verified status of an edge',
        'type'        => 'write',
        'ajax'        => true,
        'capabilities'=> 'mod/conceptmaps:editsettings'
    ),
);
