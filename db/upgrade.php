<?php
# @Date:   2020-03-26T16:53:01+01:00
# @Last modified time: 2020-03-27T09:37:59+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin upgrade steps are defined here.
 *
 * @package     mod_conceptmaps
 * @category    upgrade
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute mod_conceptmaps upgrade from the given old version.
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_conceptmaps_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    // For further information please read the Upgrade API documentation:
    // https://docs.moodle.org/dev/Upgrade_API
    //
    // You will also have to create the db/install.xml file by using the XMLDB Editor.
    // Documentation for the XMLDB Editor can be found at:
    // https://docs.moodle.org/dev/XMLDB_editor
    if ($oldversion < 2020032700) {

        // Define field grade to be added to conceptmaps.
        $table = new xmldb_table('conceptmaps');
        $field = new xmldb_field('grade', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'introformat');

        // Conditionally launch add field grade.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2020032700, 'conceptmaps');
    }

    if ($oldversion < 2020013001) {

        // Changing nullability of field start on table conceptmaps_topics to null.
        $table = new xmldb_table('conceptmaps_topics');
        $field = new xmldb_field('start', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'name');

        // Launch change of nullability for field start.
        $dbman->change_field_notnull($table, $field);

        $field = new xmldb_field('end', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'start');

        // Launch change of nullability for field start.
        $dbman->change_field_notnull($table, $field);

        $field = new xmldb_field('startcorrection', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'end');

        // Launch change of nullability for field start.
        $dbman->change_field_notnull($table, $field);

         $field = new xmldb_field('endcorrection', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'startcorrection');

        // Launch change of nullability for field start.
        $dbman->change_field_notnull($table, $field);

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2020013001, 'conceptmaps');
    }

    if ($oldversion < 2020013000) {

        // Define field submitted to be added to conceptmaps_submissions.
        $table = new xmldb_table('conceptmaps_submissions');
        $field = new xmldb_field('submitted', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0', 'version');

        // Conditionally launch add field submitted.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2020013000, 'conceptmaps');
    }

    if ($oldversion < 2020011600) {

        // Define field auto_correction to be added to conceptmaps_edges.
        $table = new xmldb_table('conceptmaps_edges');
        $field = new xmldb_field('auto_correction', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'connectionid');

        // Conditionally launch add field auto_correction.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2020011600, 'conceptmaps');
    }


    if ($oldversion < 2020011400) {

        // Changing type of field positionx on table conceptmaps_terms to int.
        $table = new xmldb_table('conceptmaps_terms');
        $field = new xmldb_field('positionx', XMLDB_TYPE_INTEGER, '20', null, null, null, null, 'editable');

        // Launch change of type for field positionx.
        $dbman->change_field_type($table, $field);

        // Changing type of field positiony on table conceptmaps_terms to int.
        $table = new xmldb_table('conceptmaps_terms');
        $field = new xmldb_field('positiony', XMLDB_TYPE_INTEGER, '20', null, null, null, null, 'positionx');

        // Launch change of type for field positiony.
        $dbman->change_field_type($table, $field);

        // Define table conceptmaps_auto_edges to be created.
        $table = new xmldb_table('conceptmaps_auto_edges');

        // Adding fields to table conceptmaps_auto_edges.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('source', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('target', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('conceptmapstopic', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('verification', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('verified', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table conceptmaps_auto_edges.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for conceptmaps_auto_edges.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }


        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2020011400, 'conceptmaps');
    }

    if ($oldversion < 2019121200) {

        // Changing type of field positionx on table conceptmaps_student_terms to char.
        $table = new xmldb_table('conceptmaps_student_terms');
        $field = new xmldb_field('positionx', XMLDB_TYPE_CHAR, '200', null, null, null, null, 'editable');

        // Launch change of type for field positionx.
        $dbman->change_field_type($table, $field);

        // Changing type of field positiony on table conceptmaps_student_terms to char.
        $table = new xmldb_table('conceptmaps_student_terms');
        $field = new xmldb_field('positiony', XMLDB_TYPE_CHAR, '200', null, null, null, null, 'positionx');

        // Launch change of type for field positiony.
        $dbman->change_field_type($table, $field);

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2019121200, 'conceptmaps');
    }

    if ($oldversion < 2019112801) {

        // Define field connectionid to be added to conceptmaps_edges.
        $table = new xmldb_table('conceptmaps_edges');
        $field = new xmldb_field('connectionid', XMLDB_TYPE_CHAR, '10', null, XMLDB_NOTNULL, null, 'con_1', 'version');

        // Conditionally launch add field connectionid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2019112801, 'conceptmaps');
    }

    if ($oldversion < 2019112600) {

        // Define field timemodified to be added to conceptmaps_submissions.
        $table = new xmldb_table('conceptmaps_submissions');
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'timecreated');

        // Conditionally launch add field timemodified.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Conceptmaps savepoint reached.
        upgrade_mod_savepoint(true, 2019112600, 'conceptmaps');
    }

    if ($oldversion < 2019111200) {

      // Define table conceptmaps_topics to be created.
        $table = new xmldb_table('conceptmaps_topics');

        // Adding fields to table conceptmaps_topics.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('conceptmapsid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('start', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('end', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('startcorrection', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('endcorrection', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('corrected', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table conceptmaps_topics.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $table->add_key('fk_conceptmapsid', XMLDB_KEY_FOREIGN, ['conceptmapsid'], 'conceptmaps', ['id']);

        // Conditionally launch create table for conceptmaps_topics.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table conceptmaps_submissions to be created.
        $table = new xmldb_table('conceptmaps_submissions');

        // Adding fields to table conceptmaps_submissions.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('conceptmapstopic', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '1');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('corrected', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('feedback', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('failed', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('version', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table conceptmaps_submissions.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $table->add_key('fk_conceptmapstopic', XMLDB_KEY_FOREIGN, ['conceptmapstopic'], 'conceptmaps_topics', ['id']);

        // Conditionally launch create table for conceptmaps_submissions.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table conceptmaps_terms to be created.
        $table = new xmldb_table('conceptmaps_terms');

        // Adding fields to table conceptmaps_terms.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('conceptmapstopic', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('editable', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('positionx', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('positiony', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table conceptmaps_terms.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $table->add_key('fk_conceptmapstopic', XMLDB_KEY_FOREIGN, ['conceptmapstopic'], 'conceptmaps_topics', ['id']);

        // Conditionally launch create table for conceptmaps_terms.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table conceptmaps_student_terms to be created.
        $table = new xmldb_table('conceptmaps_student_terms');

        // Adding fields to table conceptmaps_student_terms.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('conceptmapstopic', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('originalterm', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '1');
        $table->add_field('editable', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('positionx', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('positiony', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('version', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table conceptmaps_student_terms.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $table->add_key('fk_conceptmapstopic', XMLDB_KEY_FOREIGN, ['conceptmapstopic'], 'conceptmaps_topics', ['id']);
        $table->add_key('fk_originalterm', XMLDB_KEY_FOREIGN, ['originalterm'], 'conceptmaps_terms', ['id']);

        // Conditionally launch create table for conceptmaps_student_terms.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

       // Define table conceptmaps_edges to be created.
       $table = new xmldb_table('conceptmaps_edges');

       // Adding fields to table conceptmaps_edges.
       $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
       $table->add_field('conceptmapstopic', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
       $table->add_field('source', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
       $table->add_field('target', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
       $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '1');
       $table->add_field('content', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
       $table->add_field('verification', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
       $table->add_field('verified', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0');
       $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);
       $table->add_field('version', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

       // Adding keys to table conceptmaps_edges.
       $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
       $table->add_key('fk_conceptmapstopic', XMLDB_KEY_FOREIGN, ['conceptmapstopic'], 'conceptmaps_topics', ['id']);
       $table->add_key('fk_source', XMLDB_KEY_FOREIGN, ['source'], 'conceptmaps_student_terms', ['id']);
       $table->add_key('fk_target', XMLDB_KEY_FOREIGN, ['target'], 'conceptmaps_student_terms', ['id']);

       // Conditionally launch create table for conceptmaps_edges.
       if (!$dbman->table_exists($table)) {
           $dbman->create_table($table);
       }


       // Conceptmaps savepoint reached.
       upgrade_mod_savepoint(true, 2019111200, 'conceptmaps');
   }

    return true;
}
