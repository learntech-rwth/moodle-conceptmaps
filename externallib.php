<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External assign API
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php");

require_once($CFG->dirroot.'/mod/conceptmaps/locallib.php');

class mod_conceptmaps_external extends external_api {

    /**
     * Describes the parameters for add_auto_edge webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function add_auto_edge_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
            )
        );
    }
    /**
     * Adds the edge to the auto edges
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @return array - Warning structure containing success value
     */
    public static function add_auto_edge($conceptmapsid, $edgeid) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::add_auto_edge_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid
                                            ));
        $edgeid = $params['edgeid'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);

        // get the specified edge
        $edge = $DB->get_record_sql("SELECT e.id, e.conceptmapstopic, s.name as 'source', t.name as 'target', e.content, e.verification, e.verified, e.comment FROM mdl_conceptmaps_edges e INNER JOIN mdl_conceptmaps_student_terms s ON s.id = e.source INNER JOIN mdl_conceptmaps_student_terms t ON t.id = e.target WHERE e.id = :id", ['id'=>$edgeid]);
        // create auto edge
        // first check if already exists (source, content, target same)
        $check_edge = $DB->get_record('conceptmaps_auto_edges', ["source" => $edge->source, "target" => $edge->target, 'content' => $edge->content]);
        if($check_edge != null) {
          // if there is already a auto edge for this, do nothing, just give a warning
          $ret->success = false;
          $ret->edge_id = -1;
          $ret->edges = array();
          $ret->warning = "There is already an edge like this for the auto correction";
        } else {
          $id_auto_edge = $DB->insert_record('conceptmaps_auto_edges', $edge);

          // Update all equal edges
          $sql = "UPDATE mdl_conceptmaps_edges e JOIN mdl_conceptmaps_student_terms sc ON sc.ID = e.source JOIN mdl_conceptmaps_student_terms st ON st.ID = e.target SET auto_correction = :correction, verification = :verification, verified=0, comment= :comment WHERE sc.name = :source AND st.name = :target AND e.content = :content AND verified = 0";
          $DB->execute($sql, ['correction' => $id_auto_edge, "verification" => $edge->verification, 'comment' => $edge->comment, 'source' => $edge->source, 'target' => $edge->target, 'content' => $edge->content]);

          // get all updated edges
          $edges = $DB->get_records('conceptmaps_edges', ["auto_correction" => $id_auto_edge], '', 'id');


          $ret->success = true;
          $ret->edge_id = $id_auto_edge;
          $ret->edges = $edges;
          $ret->warning = "";
        }

        return $ret;
    }

    /**
     * Describes the return for add_auto_edge
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function add_auto_edge_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'edge_id' => new external_value(PARAM_INT),
            'edges' => new external_multiple_structure(new external_single_structure(array("id" => new external_value(PARAM_INT)))),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }

    /**
     * Describes the parameters for remove_auto_edge webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function remove_auto_edge_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
            )
        );
    }
    /**
     * Removes the edge from the auto edges
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @return array - Warning structure containing success value
     */
    public static function remove_auto_edge($conceptmapsid, $edgeid) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::remove_auto_edge_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid
                                            ));
        $edgeid = $params['edgeid'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);
        try {
          $transaction = $DB->start_delegated_transaction();
          // get the specified edge
          $edge = $DB->get_record('conceptmaps_edges', ["id" => $edgeid]);
          $auto_edge_id = $edge->auto_correction;
          if($auto_edge_id == null) {
            $ret->success = false;
            $ret->edges = array();
            $ret->warning = "The edge to remove is no auto-edge. The edge must be an auto edge";
            return $ret;
          }
          // Remove Auto_edge and all references
          $DB->delete_records('conceptmaps_auto_edges', ["id" => $auto_edge_id]);

          $edges = $DB->get_records('conceptmaps_edges', ["auto_correction" => $auto_edge_id], '', 'id');
          foreach ($edges as $key => $value) {
            $update_obj = new stdClass();
            $update_obj->id = $value->id;
            $update_obj->auto_correction = null;

            $DB->update_record('conceptmaps_edges', $update_obj);
          }


          $ret->success = true;
          $ret->edges = $edges;
          $ret->warning = "";

          $transaction->allow_commit();

          return $ret;
      }
      catch(Exception $e) {
          $ret->success = false;
          $ret->edges = array();
          $ret->warning = "Database transaction not working";

          $transaction->rollback($e);

          return $ret;
      }

    }

    /**
     * Describes the return for remove_auto_edge
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function remove_auto_edge_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'edges' => new external_multiple_structure(new external_single_structure(array("id" => new external_value(PARAM_INT)))),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }

    /**
     * Describes the parameters for change_verification webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_verification_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
                'value' => new external_value(PARAM_INT, 'The value auf the verification'),
            )
        );
    }
    /**
     * Changes the value of the verification of an edge and if exisiting the corresponding auto edge
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @param int $value - The verification value
     * @return array - Warning structure containing success value
     */
    public static function change_verification($conceptmapsid, $edgeid, $value) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::change_verification_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid,
                                                'value' => $value
                                            ));
        $edgeid = $params['edgeid'];
        $value = $params['value'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);

        if($value < 0 || $value > 3) {
          $ret->success = false;
          $ret->edges = array();
          $ret->value = $value;
          $ret->warning = "Invalid value for value (verification). It must be 0, 1, 2 or 3.";
          return $ret;
        }

        $edge = $DB->get_record('conceptmaps_edges', ["id" => $edgeid]);
        $auto_edge_id = $edge->auto_correction;
        if($auto_edge_id == null) {
          // Just change this edge
          $edge->verification = $value;
          $DB->update_record('conceptmaps_edges', $edge);
          $ret->success = true;
          $ret->edges = array();
          $ret->value = $value;
          $ret->warning = "";
        } else {
          // if there is a auto edge stated, then change the auto edge
          $auto_edge = new stdClass();
          $auto_edge->id = $auto_edge_id;
          $auto_edge->verification = $value;
          $auto_edge->comment = $edge->comment;

          $edges = conceptmaps_update_edges_of_auto($auto_edge, true);

          if($edges) {
            $ret->success = true;
            $ret->edges = $edges;
            $ret->value = $value;
            $ret->warning = "";
          } else {
            $ret->success = false;
            $ret->edges = array();
            $ret->value = -1;
            $ret->warning = "Could not update auto edge and corresponding edges";
          }
        }
        return $ret;
    }

    /**
     * Describes the return for change_verification
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_verification_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'edges' => new external_multiple_structure(new external_single_structure(array("id" => new external_value(PARAM_INT)))),
            'value' => new external_value(PARAM_INT),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }

    /**
     * Describes the parameters for change_verification webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_verification_autoedge_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
                'value' => new external_value(PARAM_INT, 'The value auf the verification'),
            )
        );
    }
    /**
     * Changes the value of the verification of an edge and if exisiting the corresponding auto edge
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @param int $value - The verification value
     * @return array - Warning structure containing success value
     */
    public static function change_verification_autoedge($conceptmapsid, $edgeid, $value) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::change_verification_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid,
                                                'value' => $value
                                            ));
        $edgeid = $params['edgeid'];
        $value = $params['value'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);

        if($value < 0 || $value > 3) {
          $ret->success = false;
          $ret->edges = array();
          $ret->value = $value;
          $ret->warning = "Invalid value for value (verification). It must be 0, 1, 2 or 3.";
          return $ret;
        }

        $edge = $DB->get_record('conceptmaps_auto_edges', ["id" => $edgeid]);
        $auto_edge_id = $edge->id;
        // change auto edge and corresponding edges
        $auto_edge = new stdClass();
        $auto_edge->id = $auto_edge_id;
        $auto_edge->verification = $value;
        $auto_edge->comment = $edge->comment;

        $edges = conceptmaps_update_edges_of_auto($auto_edge, true);

        if($edges) {
          $ret->success = true;
          $ret->edges = $edges;
          $ret->value = $value;
          $ret->warning = "";
        } else {
          $ret->success = false;
          $ret->edges = array();
          $ret->value = -1;
          $ret->warning = "Could not update auto edge and corresponding edges";
        }

        return $ret;
    }

    /**
     * Describes the return for change_verification
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_verification_autoedge_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'edges' => new external_multiple_structure(new external_single_structure(array("id" => new external_value(PARAM_INT)))),
            'value' => new external_value(PARAM_INT),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }

    /**
     * Describes the parameters for achange_comment webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_comment_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
                'value' => new external_value(PARAM_TEXT, 'The value auf the comment'),
            )
        );
    }
    /**
     * Changes the comment of an edge or if exisitng the corresponding auto edge
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @param int $value - The comment
     * @return array - Warning structure containing success value
     */
    public static function change_comment($conceptmapsid, $edgeid, $value) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::change_comment_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid,
                                                'value' => $value
                                            ));
        $edgeid = $params['edgeid'];
        $value = $params['value'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);

        // Validate comment
        $value = trim($value);
        $value = stripslashes($value);
        $value = htmlspecialchars($value);

        $edge = $DB->get_record('conceptmaps_edges', ["id" => $edgeid]);
        $auto_edge_id = $edge->auto_correction;
        if($auto_edge_id == null) {
          // Just change this edge
          $edge->comment = $value;
          $DB->update_record('conceptmaps_edges', $edge);
          $ret->success = true;
          $ret->edges = array();
          $ret->value = $value;
          $ret->warning = "";
        } else {
          // if there is a auto edge stated, then change the auto edge
          $auto_edge = new stdClass();
          $auto_edge->id = $auto_edge_id;
          $auto_edge->verification = $edge->verification;
          $auto_edge->comment = $value;

          $edges = conceptmaps_update_edges_of_auto($auto_edge, true);

          if($edges) {
            $ret->success = true;
            $ret->edges = $edges;
            $ret->value = $value;
            $ret->warning = "";
          } else {
            $ret->success = false;
            $ret->edges = array();
            $ret->value = -1;
            $ret->warning = "Could not update auto edge and corresponding edges";
          }
        }
        return $ret;
    }

    /**
     * Describes the return for change_comment
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_comment_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'edges' => new external_multiple_structure(new external_single_structure(array("id" => new external_value(PARAM_INT)))),
            'value' => new external_value(PARAM_TEXT),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }

    /**
     * Describes the parameters for achange_comment webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_comment_autoedge_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
                'value' => new external_value(PARAM_TEXT, 'The value auf the comment'),
            )
        );
    }
    /**
     * Changes the comment of an edge or if exisitng the corresponding auto edge
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @param int $value - The comment
     * @return array - Warning structure containing success value
     */
    public static function change_comment_autoedge($conceptmapsid, $edgeid, $value) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::change_comment_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid,
                                                'value' => $value
                                            ));
        $edgeid = $params['edgeid'];
        $value = $params['value'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);

        // Validate comment
        $value = trim($value);
        $value = stripslashes($value);
        $value = htmlspecialchars($value);

        $edge = $DB->get_record('conceptmaps_auto_edges', ["id" => $edgeid]);
        $auto_edge_id = $edge->id;
        // ahange autoedge and corresponding edges
        $auto_edge = new stdClass();
        $auto_edge->id = $auto_edge_id;
        $auto_edge->verification = $edge->verification;
        $auto_edge->comment = $value;

        $edges = conceptmaps_update_edges_of_auto($auto_edge, true);

        if($edges) {
          $ret->success = true;
          $ret->edges = $edges;
          $ret->value = $value;
          $ret->warning = "";
        } else {
          $ret->success = false;
          $ret->edges = array();
          $ret->value = -1;
          $ret->warning = "Could not update auto edge and corresponding edges";
        }
        return $ret;
    }

    /**
     * Describes the return for change_comment
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_comment_autoedge_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'edges' => new external_multiple_structure(new external_single_structure(array("id" => new external_value(PARAM_INT)))),
            'value' => new external_value(PARAM_TEXT),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }


    /**
     * Describes the parameters for achange_comment webservice.
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_verified_parameters() {
        return new external_function_parameters(
            array(
                'conceptmapsid' => new external_value(PARAM_INT, 'The conceptmaps id to operate on'),
                'edgeid' => new external_value(PARAM_INT, 'The edge id to operate on'),
                'value' => new external_value(PARAM_BOOL, 'The value auf the comment'),
            )
        );
    }
    /**
     * Changes the comment of an edge or if exisitng the corresponding auto edge
     *
     * @param int $conceptmapsid - The id of the conceptmaps instance
     * @param int $edgeid - The id of the edge to add
     * @param int $value - The comment
     * @return array - Warning structure containing success value
     */
    public static function change_verified($conceptmapsid, $edgeid, $value) {
        global $DB;
        $ret = new stdClass();
        $params = self::validate_parameters(self::change_verified_parameters(),
                                            array(
                                                'conceptmapsid' => $conceptmapsid,
                                                'edgeid' => $edgeid,
                                                'value' => $value
                                            ));
        $edgeid = $params['edgeid'];
        $value = $params['value'];

        list($conceptmaps, $course, $cm, $context) = self::validate_conceptmaps($conceptmapsid);
        // Just change this edge
        $edge = new stdClass();
        $edge->id = $edgeid;
        $edge->verified = $value;

        $DB->update_record('conceptmaps_edges', $edge);

        $ret->success = true;
        $ret->warning = "";

        return $ret;
    }

    /**
     * Describes the return for change_comment
     * @return external_function_parameters
     * @since  Moodle 3.1
     */
    public static function change_verified_returns() {
        return new external_single_structure(
          array(
            'success' => new external_value(PARAM_BOOL),
            'warning' => new external_value(PARAM_TEXT),
          )
        );
    }


    /**
     * Utility function for validating an conceptmaps.
     *
     * @param int $conceptmapsid conceptmaps instance id
     * @return array array containing the conceptmaps, course, context and course module objects
     * @since  Moodle 3.2
     */
    protected static function validate_conceptmaps($id) {
        global $DB;

        // Request and permission validation.
        $cm = get_coursemodule_from_id('conceptmaps', $id, 0, false, MUST_EXIST);
        $conceptmaps = $DB->get_record('conceptmaps', array('id' => $cm->instance), 'id', MUST_EXIST);
        $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);

        $context = context_module::instance($cm->id);

        self::validate_context($context);
        require_capability('mod/conceptmaps:editsettings', $context);

        return array($conceptmaps, $course, $cm, $context);
    }
}
