function startConceptmaps(yui, cmid, thema_id, experiment, conceptmapid, layout, version, nodes, kantenJS, neueVersion, doKorrektur, manu) {


  require(['jquery', 'jqueryui', 'core/notification', 'core/templates'], function($, jqui, notification, templates) {
    kantenJS = JSON.parse(kantenJS);
    var edges = [];
    var edgesToDelete = [];
    nodes = JSON.parse(nodes);


    // For todays date;
    Date.prototype.today = function() {
      return ((this.getDate() < 10) ? "0" : "") + this.getDate() + "." + (((this.getMonth() + 1) < 10) ? "0" : "") + (this.getMonth() + 1) + "." + this.getFullYear();
    }
    // For the time now
    Date.prototype.timeNow = function() {
      return ((this.getHours() < 10) ? "0" : "") + this.getHours() + ":" + ((this.getMinutes() < 10) ? "0" : "") + this.getMinutes() + ":" + ((this.getSeconds() < 10) ? "0" : "") + this.getSeconds();
    }
    jsPlumb.ready(function() {

      // Action, Time, SourceName, SourceID, TargetName, TargetID, Data1, Data2, Data3
      var past = [];
      var future = [];
      var baseTime = $.now();
      var basedate = new Date();

      // data which should be written in the log file
      var logFile = ["New beginning " + basedate.today() + "@" + basedate.timeNow()];
      var inhalt = new Array();
      var knoten = [];

      function stateName(s) {
        return $("#" + s + "-name").html();
      }

      function action(act, conn, d1, d2, d3) {
        console.log("action", act, conn, d1, d2, d3);
        var t = $.now() - baseTime;
        if(conn.id == null || conn.id == 0) {
          conn.id = conn.connection.id;
        }
        var data = {
          action: act,
          time: t,
          sourcename: stateName(conn.sourceId),
          sourceid: conn.sourceId,
          targetname: stateName(conn.targetId),
          targetid: conn.targetId,
          connectionid: conn.id
        };
        if (d1 !== null && d1 !== undefined) data.d1 = d1;
        if (d2 !== null && d2 !== undefined) data.d2 = d2;
        if (d3 !== null && d3 !== undefined) data.d3 = d3;
        return data;
      }

      function nodeAction(act, node, d1, d2, d3) {
        var t = $.now() - baseTime;
        var data = [act, t, stateName(node), node];
        if (d1 !== null && d1 !== undefined) data.push(d1);
        if (d2 !== null && d2 !== undefined) data.push(d2);
        if (d3 !== null && d3 !== undefined) data.push(d3);
        return data;
      }

      function checkTime() {
        var curTime = $.now();
        var diff = Math.floor((curTime - baseTime) / 1000);
        var min = Math.floor(diff / 60);
        var sek = Math.floor(diff % 60);
        if (min == 10 && sek == 0) alert("Bitte kommen Sie zum Ende.");
        if (sek < 10) sek = '0' + sek;
        $('#zeit').html(min + ':' + sek);
      }


      // TEST David
      $("#david").click(function() {

        var canvas = document.createElement('canvas');
        canvas.setAttribute("id", "canvasForExport");
        canvas.height = $("#conceptmap").height();
        canvas.width = ($("#conceptmap").width());
        var ctx = canvas.getContext('2d');
        // Wandelt die SVG Arrows in Canvas Elemente um und tut Sie an die gleiche Stelle.
        $("#conceptmap > svg").each(function() {
          var top = this.style.top;
          var left = this.style.left;
          $('#conceptmap').append(canvas);
          var svgTag = new XMLSerializer().serializeToString(this);
          ctx.drawSvg(svgTag, left, top);
        });

        html2canvas($("#conceptmap"), {
          onrendered: function(canvas) {
            var img = canvas.toDataURL();
            //				var pdf = img.replace("image/png" ,"application/pdf");
            var link = document.createElement("a");
            link.download = experiment + ".png";
            link.href = img;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            // Entfernt den link und die doppelten Pfeile
            delete link;
            document.getElementById("canvasForExport").remove();
          }
        });
        return false;
      });

      function logAction(action) {
        //todo: rework
        console.log("Did ", action);
        logFile.push(new Date().today() + "@" + new Date().timeNow() + ": Did " + action);
        if (action.action == "connect") {
          //edge will be saved in content. If the label will be changes, also the edge will be changed.
          //tmp = [begriffname src, id src, begriffname tar,id tar, neuer Inhalt, ID der Kante (-1 für noch keine ID vorhanden),Status (gelöscht,geändert,neu,alt)
          var tmp = [action.sourcename, action[3], action[4], action[5], "", -1, "neu"];

          //inhalt.push(tmp);
          var edge = {
            connectionid: action.connectionid || action.connection.id ,
            sourceid: action.sourceid.substring(5),
            targetid: action.targetid.substring(5),
            content: ""
          };
          edges.push(edge);

        } else if (action.action == "rename") {

          for (var i in edges) {
            if(edges[i].connectionid == action.connectionid) {
              edges[i].content = action.d2 || action.content;
              break;
            }
          }
        } else if (action.action == "rename-node") {
          for(var i in nodes) {
            if (nodes[i].id == action.id) {
              nodes[i].name = action.content;
              break;
            }
          }
        }
        past.push(action);
      }

      function doAction(action) {
        console.log("Doing " + action);
        logFile.push(new Date().today() + "@" + new Date().timeNow() + ": Doing " + action);
        if (action[0] == "connect") {
          var conn = instance.connect({
            source: action[3],
            target: action[5]
          });
          past.pop();
        } else if (action.action == "detach") {

          var edge = {
            sourceid: action.sourceid,
            targetid: action.targetid,
            content: action.content,
            connectionid: action.connectionid
          }

          var conns = instance.getConnections({
            source: action.sourceid,
            target: action.targetid
          });
          for (var i = 0; i < conns.length; i++) {
            if (conns[i].id == action.connectionid) {
              jsPlumb.detach(conns[i]);
              break;
            }
          }
          edgesToDelete.push(edge);
          var index = -1;
          // remove form edges array
          for(var i in edges) {
            if(edges[i].connectionid == action.connectionid) {
              index = i;
              break;
            }
          }
          if(index != -1) edges.splice(index,1);

        }
        past.push(action);
      }

      function undoAction(action) {
        //TODO: rework, and make it usable
        console.log(new Date().today() + "@" + new Date().timeNow() + ": Undoing " + action);
        if (action[0] == "connect") {
          var c = instance.getConnections({
            source: action[3],
            target: action[5]
          })[0];
          jsPlumb.detach(c);
          future.push(action);
        } else if (action[0] == "detach") {
          var c = instance.connect({
            source: action[3],
            target: action[5]
          });
          future.push(action);
        } else if (action[0] == "rename") {
          var c = instance.getConnections({
            source: action[3],
            target: action[5]
          })[0];
          c.getOverlay("label").setLabel(action[6]);
          future.push(action);
        } else if (action[0] == "rename-node") {
          var label = $("#" + action[3]);
          //TODOlabel.editable("hide");
          //label.editable("setValue", action[4]);
          future.push(action);
        }
      }

      // setup some defaults for jsPlumb.
      var instance = jsPlumb.getInstance({
        Endpoint: ["Dot", {
          radius: 2
        }],
        /*HoverPaintStyle: {strokeStyle: "#1e8151", lineWidth: 2 },*/
        ConnectionOverlays: [
          ["Arrow", {
            location: 1,
            id: "arrow",
            length: 14,
            foldback: 0.8
          }],
          ["Label", {
            label: "",
            id: "label",
            cssClass: "aLabel edit",
            location: 0.6
          }],
          ["Label", {
            label: "X",
            id: "delete",
            cssClass: "aLabel",
            location: 0.25,
            labelStyle: {
              color: "red"
            },
            events: {
              click: function(overlay, event) {
                var c = overlay.component;
                doAction(action("detach", c, c.getOverlay("label").getLabel(), c.id));
              }
            }
          }]
        ],
        Container: "conceptmap"
      });

      //TODO: add zoom
      let zoomLevel = 1.0;

      $("#conceptmap").css({
          "-webkit-transform":"scale("+zoomLevel+")",
          "-moz-transform":"scale("+zoomLevel+")",
          "-ms-transform":"scale("+zoomLevel+")",
          "-o-transform":"scale("+zoomLevel+")",
          "transform":"scale("+zoomLevel+")"
      });
      jsPlumb.setZoom(zoomLevel);

      window.setZoom = function(zoom, instance, transformOrigin, el) {
        transformOrigin = transformOrigin || [0.5, 0.5];
        instance = instance || jsPlumb;
        el = el || instance.getContainer();
        var p = ["webkit", "moz", "ms", "o"],
          s = "scale(" + zoom + ")",
          oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%";

        for (var i = 0; i < p.length; i++) {
          el.style[p[i] + "Transform"] = s;
          el.style[p[i] + "TransformOrigin"] = oString;
        }

        el.style["transform"] = s;
        el.style["transformOrigin"] = oString;

        instance.setZoom(zoom);
      };
      setZoom(zoomLevel, instance, [0.0, 0.0]);

      $('#zoomPlus').click(function() {
        if (zoomLevel <= 1.9) {
          zoomLevel += 0.1;
        }
        setZoom(zoomLevel, instance, [0.0, 0.0]);
      });
      $('#zoomMinus').click(function() {
        if (zoomLevel >= 0.1) {
          zoomLevel -= 0.1;
        }
        setZoom(zoomLevel, instance, [0.0, 0.0]);
      });
      $('#zoom100').click(function() {
        zoomLevel = 1.0;
        setZoom(zoomLevel, instance, [0.0, 0.0]);
      });


      window.jsp = instance;
      var windows = jsPlumb.getSelector(".conceptmap .w");

      function findEmptyLabels() {
        var res = [];
        var conns = instance.getConnections();
        for (var i = 0; i < conns.length; i++) {
          var ol = conns[i].getOverlay("label");
          var str = ol.canvas.innerHTML;
          if (str == "Empty") res.push(ol.canvas);
        }
        return res;
      }

      function findEmptyNodes() {
        var res = [];
        for (var i = 0; i < nodes.length; i++) {
          var id = nodes[i].id;
          var begriff = nodes[i].name;
          if (begriff == "Empty" || begriff == "") {
            var div = document.getElementById(id);
            res.push(div);
          }

        }
        return res;
      }

      function findDoubleConnections() {
        var res = [];
        var conns = instance.getConnections();
        for (var i = 0; i < conns.length - 1; i++) {
          for (var j = i + 1; j < conns.length; j++) {
            if (conns[i].sourceId == conns[j].sourceId && conns[i].targetId == conns[j].targetId) {
              res.push([conns[i], conns[j]]);
            }
          }
        }
        return res;
      }

      function finish(save) {
        $('.w').css("background-color", "white");
        //make color normal again
        var conns = instance.getConnections();
        for (var i = 0; i < conns.length; i++) {
          conns[i].setPaintStyle({
            strokeStyle: "#5c96bc",
            lineWidth: 2,
            outlineColor: "transparent",
            outlineWidth: 2
          });
        }
          //get position of each node
          for (var i = 0; i < nodes.length; i++) {
            var id_div = nodes[i].id;
            var css = $('#state' + id_div).css(['left', 'top']);
            var left = css['left'];
            var top = css['top'];
            nodes[i].positionx = left;
            nodes[i].positiony = top;
          }


        var doubleEdges = findDoubleConnections();
        var empty = findEmptyLabels();
        var emptyNodes = findEmptyNodes();
        if (doubleEdges.length > 0) {
          for (var i = 0; i < doubleEdges.length; i++) {
            doubleEdges[i][0].setPaintStyle({
              strokeStyle: "#F73939",
              lineWidth: 2,
              outlineColor: "transparent",
              outlineWidth: 2
            });
            doubleEdges[i][1].setPaintStyle({
              strokeStyle: "#F73939",
              lineWidth: 2,
              outlineColor: "transparent",
              outlineWidth: 2
            });

            doubleEdgesError();
          }

        } else {
          if ((empty.length > 0 || emptyNodes.length > 0) && save == 0) {
            notification.alert(M.util.get_string('emptyEdgesAlert', 'conceptmaps'), M.util.get_string('emptyEdgesMessage', 'conceptmaps') , M.util.get_string('yesButton', 'conceptmaps'));

            $.each(empty, function(i, e) {
              $(e).css("background-color", "rgba(255, 0, 0, 0.4)");
            });
            $.each(emptyNodes, function(i, e) {
              $(e).css("background-color", "rgba(255, 0, 0, 0.4)");
            });

          } else {

            if (neueVersion) {
              layout = 0; //no layout
            }
            if (save === 2) { //Submit
              logFile.push("Submitted at " + new Date().today() + "@" + new Date().timeNow());
              var callback = function () { //Could be outsourced!
                $.ajax({
                  type: "POST",
                  url: "createConceptmap.php",
                  data: {
                    "id": cmid,
                    "conceptmapid": conceptmapid,
                    "session": experiment,
                    "finish": "1",
                    "edges": JSON.stringify(edges),
                    "edges_to_delete": JSON.stringify(edgesToDelete),
                    "nodes": JSON.stringify(nodes),
                    "topicid": thema_id,
                    "edited": 1,
                    "version": version,
                    "submitted": 1,
                    "logFile": logFile
                  },

                  success: function(data, status, xhr) {
                    $(location).attr("href", "view.php?id="+cmid);
                  },
                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    $("#submitConceptmap").removeClass("navbarlinkdisabled");
                    $("#save").removeClass("navbarlinkdisabled");
                  }

                });
              }
              notification.confirm(M.util.get_string('submittingConceptmap', 'conceptmaps'), M.util.get_string('confirmSubmitting', 'conceptmaps') , M.util.get_string('yesButton', 'conceptmaps'), M.util.get_string('cancelButton', 'conceptmaps'), callback, null);

            } else if (save === 0) { //save

              logFile.push("Saved at " + new Date().today() + "@" + new Date().timeNow());
              var callback = function () { //Could be outsourced!
                $.ajax({
                  type: "POST",
                  url: "createConceptmap.php",
                  data: {
                    "id": cmid,
                    "conceptmapid": conceptmapid,
                    "session": experiment,
                    "finish": "1",
                    "edges": JSON.stringify(edges),
                    "edges_to_delete": JSON.stringify(edgesToDelete),
                    "nodes": JSON.stringify(nodes),
                    "topicid": thema_id,
                    "edited": 1,
                    "version": version,
                    "submitted": 0,
                    "logFile": logFile
                  },

                  success: function(data, status, xhr) {
                    $(location).attr("href", "view.php?id="+cmid);
                  },
                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    $("#submitConceptmap").removeClass("navbarlinkdisabled");
                    $("#save").removeClass("navbarlinkdisabled");
                  }

                });
              }
              notification.confirm(M.util.get_string('SaveAndBack', 'conceptmaps'), M.util.get_string('confirmSaveAndBack', 'conceptmaps') , M.util.get_string('yesButton', 'conceptmaps'), M.util.get_string('cancelButton', 'conceptmaps'), callback, null);
            } else {

              logFile.push("Automatic save at " + new Date().today() + "@" + new Date().timeNow());


              $("#submitConceptmap").addClass("navbarlinkdisabled");
              $("#save").addClass("navbarlinkdisabled");

              $.ajax({
                type: "POST",
                url: "createConceptmap.php",
                data: {
                  "id": cmid,
                  "conceptmapid": conceptmapid,
                  "session": experiment,
                  "finish": "1",
                  "edges": JSON.stringify(edges),
                  "edges_to_delete": JSON.stringify(edgesToDelete),
                  "nodes": JSON.stringify(nodes),
                  "topicid": thema_id,
                  "edited": 1,
                  "version": version,
                  "submitted": false,
                  "logFile": logFile
                },

                success: function(data, status, xhr) {
                  //Die neuen IDs der neu angelegten Kanten muss in inhalt gespeichert werden
                  try {
                    data = JSON.parse(data);
                    if (data.status === "Error") {
                      notification.addNotification({
                        message: M.util.get_string('conceptmap_saved_error', 'conceptmaps'),
                        type: "error"
                      });
                      $("#submitConceptmap").removeClass("navbarlinkdisabled");
                      $("#save").removeClass("navbarlinkdisabled");
                    } else {
                      notification.addNotification({
                        message: M.util.get_string('conceptmap_saved_success', 'conceptmaps'),
                        type: "success"
                      });
                      setTimeout(function(){
                        let notificationpanel = document.getElementById("user-notifications");
                        while (notificationpanel.hasChildNodes()) {
                            notificationpanel.removeChild(notificationpanel.firstChild);
                        } 
                      }, 5000);
                      $("#submitConceptmap").removeClass("navbarlinkdisabled");
                      $("#save").removeClass("navbarlinkdisabled");
                    }
                  } catch (SyntaxError) {

                    $("#submitConceptmap").removeClass("navbarlinkdisabled");
                    $("#save").removeClass("navbarlinkdisabled");
                    notification.addNotification({
                      message: M.util.get_string('conceptmap_saved_error', 'conceptmaps'),
                      type: "error"
                    });
                  }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                  alert("Status: " + textStatus);
                  alert("Error: " + errorThrown);
                  $("#submitConceptmap").removeClass("navbarlinkdisabled");
                  $("#save").removeClass("navbarlinkdisabled");
                }

              });
            }

          }
        }

      }
      $("#save").click(() => finish(0));
      $("#submitConceptmap").click(submitConceptmap);

      function submitConceptmap() {
        finish(2);
      }

      function zwischenspeichern() {
        finish(1);
      }



      // initialise draggable elements.
      instance.draggable(windows, {
        containment: "parent",
        handle: ".w-drag"
      });

      function doesIdExistAlready(id) {
        var connections = instance.getConnections();
        var count = 0;
        for( var i in connections) {
          if(connections[i].id == id) {
            count++;
          }
        }
        if(count > 1) {
          return true;
        }

        // test edge array
        for( var i in edges) {
          if(edges[i].connectionid == id) {
            return true;
          }
        }
        return false;
      }

      // On create connection
      instance.bind("connection", function(info, original) {
        // test if id exisits already (would cause trouble)
        if(doesIdExistAlready(info.connection.id)) {
          do {
            var number = info.connection.id.substring(4);
            number++;
            info.connection.id = "con_"+number;
          }while (doesIdExistAlready(info.connection.id))
        }
        logAction(action("connect", info));
        instance.recalculateOffsets("conceptmap");
        instance.repaintEverything();
        var label = info.connection.getOverlay("label").getElement();
        if (original) {

          $(label).on("init", function(e, editable) {
            window.setTimeout(function() {
              // TODO open inplace_editable, so that user can directly write the content of the edge!

            }, 100);
          });
        }

        window.setTimeout(function() {
          var displayvalue = "<span style='font-style:italic;'>Empty</span>";
          var value = "";
          if(label.innerHTML) {
            displayvalue = label.innerHTML;
            value = label.innerHTML;
          }
          // TODO open inplace_editable, so that user can directly write the content of the edge!
          var context = {
            "displayvalue" : displayvalue,
            "value" : value,
            "itemid" : info.connection.id,
            "component" : "mod_conceptmaps",
            "itemtype" : "conceptmaps_edge",
            "edithint" : "Edit this", //TODO
            "editlabel" : "New name for this", //TODO
            "type" : "text",
            "options" : "",
            "linkeverything": 1
          }
          templates.render('core/inplace_editable', context)
            .then(function(html, js) {
              //label.innerHTML = html;
              templates.replaceNodeContents(label, html, js);
              templates.runTemplateJS(js);
            });
        }, 0);

      });

      // For inplace_editable
      $('body').on('updated', '[data-inplaceeditable]', function(e) {
        var ajaxreturn = e.ajaxreturn; // Everything that web service returned.
        var oldvalue = e.oldvalue; // Element value before editing (note, this is raw value and not display value).
        if(ajaxreturn.itemtype == "conceptmaps_term") {
          var action = {
            action: "rename-node",
            id: ajaxreturn.itemid,
            content: ajaxreturn.value
          };
          logAction(action);
        } else if(ajaxreturn.itemtype == "conceptmaps_edge"){
          var action = {
            action: "rename",
            connectionid: ajaxreturn.itemid,
            content: ajaxreturn.value
          }
          logAction(action);
        }


      });


      // suspend drawing and initialise.
      instance.batch(function() {
        instance.makeSource(windows, {
          filter: ".ep",
          anchor: "Continuous",
          connector: ["Straight"],
          connectorStyle: {
            strokeStyle: "#5c96bc",
            lineWidth: 2,
            outlineColor: "transparent",
            outlineWidth: 2
          },
          maxConnections: 5,
          onMaxConnections: function(info, e) {
            alert("Maximum connections (" + info.maxConnections + ") reached");
          }
        });

        // initialise all '.w' elements as connection targets.
        instance.makeTarget(windows, {
          dropOptions: {
            hoverClass: "dragHover"
          },
          anchor: "Continuous",
          allowLoopback: false
        });

      });

      if ($("#zeit").length) {
        setInterval(checkTime, 500);
      }


      function loesung(loese) {
        if (loese) {
          $('#conceptmap').addClass("disabledbutton");
        }
        //Knoten verbinden
        for(var i in kantenJS) {

          jsPlumb.ready(function() {

            var con = instance.connect({
              source: "state" + kantenJS[i].sourceid,
              target: "state" + kantenJS[i].targetid,
              container: $('.plumbing')
            });

            // give the connection the old connection id
            if(edges[i].connectionid == con.id) {
              edges[i].connectionid = kantenJS[i].connectionid;
            } else {
              for(var j in edges) {
                if(edges[j].connectionid == con.id) {
                  edges[j].connectionid = kantenJS[i].connectionid;
                }
              }
            }

            con.id = kantenJS[i].connectionid;
            // change id of connection object
            $(con).attr('id', kantenJS[i].connectionid);
            con.getOverlay("label").getElement().innerHTML = kantenJS[i].content;
            var action = {
              action: "rename",
              time: 123456,
              sourceid: con.sourceId,
              targetid: con.targetId,
              content:  kantenJS[i].content,
              connectionid: con.id
            };
            logAction(action);
            /*if (loese) {
              con.getOverlay("delete").hide();
            }

            if (manu || neueVersion) {

              makeHoverManu(con, i, edges);
            }
*/
          });

        }
        $('.aLabel').each(function() {
          if ($(this)[0].innerHTML == "X") {

          } else {
            $(this).css({
              color: "black"
            });
          }
        });
        instance.recalculateOffsets("conceptmap");
        instance.repaintEverything();
        instance.repaintEverything();
      }

      switch(layout) {
        case 1:
          layout_first();
          break;
        case 2:
          loesung(false);
          break;
        case 3:
          loesung(true);
          break;
        default: layout_first();
      }


      function layout_first() {
        var x = 0;
        var y = 0;
        var buffer = 10;
        var height = 50;
        for(var i in nodes) {
          $('#state' + nodes[i].id).css({'left': x, 'top': y});
          nodes[i].positionx = x;
          nodes[i].positiony = y;
          y += buffer + height;
        }
        zwischenspeichern();
      }

      function korrektur() {
        $('#conceptmap').removeClass("disabledbutton");
        $('.ep').addClass("disabledbutton");
        $('._jsPlumb_overlay').addClass("disabledbutton");
        $('.aLabel').addClass("disabledbutton");
        var cons=instance.getConnections();

        for(var i = 0; i < kantenJS.length; i++) {
          var edge = kantenJS[i];
          let con = null;
          for(index in cons) {
            if(cons[index].id == edge.connectionid) {
              con = cons[index];
            }
          }
          var color = "";
          switch(edge.verification) {
            case "0":
              color = "#F73939";
              break;
            case "1":
              color = "#EDE913";
              break;
            case "2":
              color = "#EDE913";
              break;
            case "3":
              color = "#13ED34";
              break;
            default:
              color = "#F73939";
          }
          if(con !== null) {
            con.setPaintStyle({
              strokeStyle: color,
              lineWidth: 2,
              outlineColor: "transparent",
              outlineWidth: 2
            });
            con.getOverlay("delete").hide();
            //con.getOverlay("label").disable();
            makeHoverManu(con, edge);
          }
        }


        //Wenn Evaluierung ist, gibt es keine falschen/fehlenden/richtige Kanten, dafür aber alle Kanten. Und eigene Kanten, wenn es ein User aufruft.
        /*
        for (var i = 0; i < alleKanten.length; i++) {
          jsPlumb.ready(function() {
            var maxWidth = 10;

            var con = instance.connect({
              source: "state" + alleKanten[i][0],
              target: "state" + alleKanten[i][1],
            });

            if (max > maxWidth) {
              var anzahl = alleKanten[i][2] * maxWidth / max;
            } else {
              var anzahl = alleKanten[i][2];
            }

            con.getOverlay("label").getElement().innerHTML = alleKanten[i][2];

            if (alleKanten[i][3] != false) {
              con.setPaintStyle({
                strokeStyle: "#13ED34",
                lineWidth: anzahl,
                outlineColor: "transparent",
                outlineWidth: 1
              });
              con.getOverlay("label").canvas.style.backgroundColor = "#13ED34";
              con.getOverlay("delete").canvas.style.backgroundColor = "#13ED34";
            } else {
              con.setPaintStyle({
                strokeStyle: "#5c96bc",
                lineWidth: anzahl,
                outlineColor: "transparent",
                outlineWidth: 1
              });
            }

            con.getOverlay("label").hide();
            con.getOverlay("delete").hide();

            setConnectionLabel(con, alleKanten[i][2]);

          });
        }*/
        $('.aLabel').each(function() {
          if ($(this)[0].innerHTML == "X") {

          } else {
            $(this).css({
              color: "black"
            });
            $(this).addClass("disabledbutton");
          }
        });

      }
      if (doKorrektur) {
        korrektur();
      }


      function setConnectionLabel(connection, label) {
        connection.bind("mouseover", function(conn) {
          conn.getOverlay("label").show();
          conn.getOverlay("delete").show();

        });

        connection.bind("mouseout", function(conn) {
          conn.getOverlay("label").hide();
          conn.getOverlay("delete").hide();
        });
      }

      function makeHoverManu(con, edge) {
        var verificationWords = ["formal und inhaltlich falsch", "formal falsch", "inhaltlich falsch", "formal und inhaltlich richtig"];
        var verificationFlag = edge.verification;
        var status = verificationWords[verificationFlag];

        var comment = edge.comment;
        if (comment == null) {
          comment = "";
        } else {
          comment = "<br>" + comment;
        }


        var labelID = con.getOverlay("label").canvas.id;
        var labelDiv = document.getElementById(labelID);
        labelDiv.className += " tooltipKorrektur";

        var hoverDiv = document.createElement("SPAN");
        hoverDiv.id = "kante_" + edge.id;
        hoverDiv.innerHTML = status + "<br>" + comment;
        hoverDiv.className += " tooltipKorrekturInhalt";
        labelDiv.appendChild(hoverDiv);
      }


      if (!doKorrektur) {
        var min = 2;
        var sec = min * 60;
        //Saves the conceptmap in the background every two minutes
        setInterval(zwischenspeichern, (sec * 1000));
      }

    });

  });
}
