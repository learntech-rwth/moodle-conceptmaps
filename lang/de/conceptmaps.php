<?php
# @Date:   2019-11-05T11:40:53+01:00
# @Last modified time: 2020-03-10T15:24:16+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_conceptmaps
 * @category    string
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Concept Maps';
$string['modulename'] = 'Concept Maps';
$string['modulenameplural'] = 'Concept Maps';
$string['pluginadministration'] = 'Concept Maps Administration';

$string['conceptmapsname'] = 'Name des Conceptmap Tools';
$string['conceptmapssettings'] = 'Einstellungen der Conceptmaps Instanz';
$string['conceptmapsfieldset'] = 'Fieldset of Conceptmaps instance';
$string['conceptmapsname_help'] = 'Hilfe';

$string['overview'] = 'Übersicht';
$string['topics_overview'] = 'Themen';
$string['submissions'] = 'Abgaben';
$string['autocorrection'] = 'Autokorrektur';
$string['groups'] = 'Gruppen';
$string['help'] = 'Hilfe';

$string['strftimedate'] = '%d %b %y, %I:%M %p';

$string['noTopicsAvailable'] = 'Momentan keine Themen vorhanden!';
$string['createTopic'] = 'Erstelle neues Thema';
$string['topic'] = "Thema";
$string['topic_name'] = 'Name des Themas';
$string['header_date'] = 'Datumseinstellungen';
$string['topic_start'] = 'Start';
$string['topic_end'] = 'Ende';
$string['topic_startcorrection'] = 'Start der Korrektur';
$string['topic_endcorrection'] = 'Ende der Korrektur';

$string['header_terms'] = 'Begriffe';
$string['topic_term'] = 'Begriff {$a}';

$string['topic_create_submit'] = 'Erstellen';
$string['topic_edit_submit'] = 'Speichern';
$string['topic_term_empty_count'] = 'Anzahl der leeren Begriffe';
$string['store_successful'] = 'Erstellen war erfolgreich';
$string['edit'] = 'Editieren';
$string['view'] = 'Ansehen';

$string['save_successful'] = 'Erfolgreich gespeichert';

$string['conceptmap_saved_success'] = 'Concept Map wurde erfolgreich gespeichert.';
$string['conceptmap_saved_error'] = 'Fehler beim Speichern der Concept Map';
$string['zoomPlus'] = 'hinein zoomen';
$string['zoomMinus'] = 'heraus zoomen';


$string['editconceptmapterm'] = "Begriff bearbeiten";
$string['newtermfor'] = 'Neuer Name für {$a}';
$string['submittingConceptmap'] = "Concept Map abgeben";
$string['confirmSubmitting'] = "Sind Sie sicher, dass sie die Concept Map abgeben wollen? Danach können Sie die Concept Map nicht weiter bearbeiten.";
$string['yesButton'] = "Ja";
$string['cancelButton'] = "Abbrechen";
$string['SaveAndBack'] = "Speichern und zurück zur Übersicht";
$string['confirmSaveAndBack'] = "Möchten Sie wirklich verlassen? Dies speichert lediglich die Conceptmap. Zum Abgeben, drücken Sie bitte auf \"Abgeben\"";
$string['topic_count_empty_terms_notnumeric'] = "Die Anzahl der leeren Begriffe muss eine Zahl sein";
$string['topic_name_missing'] = "Ein Name für das Thema ist erforderlich";
$string['submissionFor'] = 'Abgabe für {$a}';
$string['user'] = "Benutzer";
$string['version'] = "Version";
$string['save'] = "Speichern";
$string['submit'] = "Abgeben";
$string['redoCheckbox'] = 'Als "erneut bearbeiten" markieren';
$string['correctedCheckbox'] = "Als korrigiert markieren";
$string['correction'] = "Korrektur";
$string['singleEdges'] = "Einzelverbindungen";
$string['conceptmap_auto_edge_already_exists'] = "Diese Autoverbindung exisiert bereits";
$string['conceptmap_change_verification_failed'] = "Das Ändern der Verifikation ist fehlgeschlagen";
$string['toUnverified'] = "Zu nicht verfizieren Verbindungen";
$string['toVerified'] = "Zu verifizierten Verbindungen";
$string['status'] = "Status";
$string['correctionOfSingleConnections'] = "Korrektur der Einzelverbindungen";

$string['name'] = "Name";
$string['endcorrection'] = "Korrektur";
$string['submission'] = "Abgabe";
$string['show'] = "zeigen";
$string['nothing'] = "nichts";
$string['to'] = "zu";
$string['topics'] = "Themen";
$string['overview'] = "Übersicht";
$string['noTopics'] = "Keine Themen verfügbar";
$string['delete'] = "Löschen";
$string['emptyEdgesAlert'] = 'Leere Begriffe oder Verbindungen!';
$string['emptyEdgesMessage'] = 'Sie haben leere Begriffe oder Verbindungen ohne Inhalt. Bitte füllen Sie diese bevor sie gehen.';

$string['source'] = 'Von';
$string['content'] = 'Inhalt';
$string['target'] = 'Zu';
$string['comment'] = 'Kommentar';
$string['corrected'] = 'korrigiert';
$string['check'] = 'Check';

$string['username'] = 'Teilnehmername';
$string['add'] = 'Hinzufügen';
$string['content_correct'] = 'inhaltlich';
$string['formal_correct'] = 'formal';

$string['editSubmitted'] = 'bereits abgegeben';

$string['status_active'] = 'aktiv';
$string['status_corr_active'] = 'Korrektur aktive';
$string['status_upcoming'] = 'bevorstehend';
$string['status_wait_submission'] = 'auf Abgabe warten';
$string['status_i_corr_active'] = '{$a}. Korrektur aktiv';
$string['status_nothing_submitted'] = 'Nichts abgegeben';
$string['status_completed'] = 'abgeschlossen';
$string['status_undefined'] = 'undefiniert';
$string['status_corr_upcoming'] = 'Korrektur bevorstehend';
$string['status_resubmit'] = 'Bitte erneut einreichen';

$string['singleCorrection_status'] = 'Status';
$string['singleCorrection_status_help'] = 'Dies ist die Korrektur der einzelnen Verbindungen. Hier können Sie angeben, ob diese sowohl formal als auch inhaltlich korrekt sind.';
$string['toOverview'] = 'Zur Übersicht';
