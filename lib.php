<?php
# @Date:   2020-03-26T16:52:57+01:00
# @Last modified time: 2020-03-27T09:28:39+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function conceptmaps_supports($feature) {
    switch ($feature) {
        case FEATURE_GRADE_HAS_GRADE:
            return null;
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the mod_conceptmaps into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $moduleinstance An object from the form.
 * @param mod_conceptmaps_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function conceptmaps_add_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timecreated = time();

    $id = $DB->insert_record('conceptmaps', $moduleinstance);

    return $id;
}

/**
 * Updates an instance of the mod_conceptmaps in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_conceptmaps_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function conceptmaps_update_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    return $DB->update_record('conceptmaps', $moduleinstance);
}

/**
 * Removes an instance of the mod_conceptmaps from the database.
 *
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function conceptmaps_delete_instance($id) {
    global $DB;

    $exists = $DB->get_record('conceptmaps', array('id' => $id));
    if (!$exists) {
        return false;
    }

    $DB->delete_records('conceptmaps', array('id' => $id));

    return true;
}

/**
 * Is a given scale used by the instance of mod_conceptmaps?
 *
 * This function returns if a scale is being used by one mod_conceptmaps
 * if it has support for grading and scales.
 *
 * @param int $moduleinstanceid ID of an instance of this module.
 * @param int $scaleid ID of the scale.
 * @return bool True if the scale is used by the given mod_conceptmaps instance.
 */
function conceptmaps_scale_used($moduleinstanceid, $scaleid) {
    global $DB;

    if ($scaleid && $DB->record_exists('conceptmaps', array('id' => $moduleinstanceid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if scale is being used by any instance of mod_conceptmaps.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param int $scaleid ID of the scale.
 * @return bool True if the scale is used by any mod_conceptmaps instance.
 */
function conceptmaps_scale_used_anywhere($scaleid) {
    global $DB;

    if ($scaleid and $DB->record_exists('conceptmaps', array('grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Creates or updates grade item for the given mod_conceptmaps instance.
 *
 * Needed by {@link grade_update_mod_grades()}.
 *
 * @param stdClass $moduleinstance Instance object with extra cmidnumber and modname property.
 * @param bool $reset Reset grades in the gradebook.
 * @return void.
 */
function conceptmaps_grade_item_update($moduleinstance, $reset=false) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    $item = array();
    $item['itemname'] = clean_param($moduleinstance->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;

    if ($moduleinstance->grade > 0) {
        $item['gradetype'] = GRADE_TYPE_VALUE;
        $item['grademax']  = $moduleinstance->grade;
        $item['grademin']  = 0;
    } else if ($moduleinstance->grade < 0) {
        $item['gradetype'] = GRADE_TYPE_SCALE;
        $item['scaleid']   = -$moduleinstance->grade;
    } else {
        $item['gradetype'] = GRADE_TYPE_NONE;
    }
    if ($reset) {
        $item['reset'] = true;
    }

    grade_update('/mod/conceptmaps', $moduleinstance->course, 'mod', 'conceptmaps', $moduleinstance->id, 0, null, $item);
}

/**
 * Delete grade item for given mod_conceptmaps instance.
 *
 * @param stdClass $moduleinstance Instance object.
 * @return grade_item.
 */
function conceptmaps_grade_item_delete($moduleinstance) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    return grade_update('/mod/conceptmaps', $moduleinstance->course, 'mod', 'conceptmaps',
                        $moduleinstance->id, 0, null, array('deleted' => 1));
}

/**
 * Update mod_conceptmaps grades in the gradebook.
 *
 * Needed by {@link grade_update_mod_grades()}.
 *
 * @param stdClass $moduleinstance Instance object with extra cmidnumber and modname property.
 * @param int $userid Update grade of specific user only, 0 means all participants.
 */
function conceptmaps_update_grades($moduleinstance, $userid = 0) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    // Populate array of grade objects indexed by userid.
    $grades = array();
    grade_update('/mod/conceptmaps', $moduleinstance->course, 'mod', 'conceptmaps', $moduleinstance->id, 0, $grades);
}

/**
 * Extends the global navigation tree by adding mod_conceptmaps nodes if there is a relevant content.
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $conceptmapsnode An object representing the navigation tree node.
 * @param stdClass $course.
 * @param stdClass $module.
 * @param cm_info $cm.
 */
function conceptmaps_extend_navigation($conceptmapsnode, $course, $module, $cm) {
}

/**
 * Extends the settings navigation with the mod_conceptmaps settings.
 *
 * This function is called when the context for the page is a mod_conceptmaps module.
 * This is not called by AJAX so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@link settings_navigation}
 * @param navigation_node $conceptmapsnode {@link navigation_node}
 */
function conceptmaps_extend_settings_navigation($settingsnav, $conceptmapsnode = null) {
}


/**
 *
 */
function conceptmaps_inplace_editable($itemtype, $itemid, $newvalue) {
  global $DB, $USER, $CFG;
  if ($itemtype === 'conceptmaps_term') {
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/term.php');
      $record = $DB->get_record('conceptmaps_student_terms', array('id' => $itemid), '*', MUST_EXIST);
      // Must call validate_context for either system, or course or course module context.
      // This will both check access and set current context.
      \external_api::validate_context(context_system::instance());
      // Check permission of the user to update this item.
      if($USER->id !== $record->userid) {
        $redirectionTarget = new moodle_url('/login/index.php');
        redirect($redirectionTarget->out());
      }
      // Clean input and update the record.
      $newvalue = clean_param($newvalue, PARAM_NOTAGS);

      $DB->update_record('conceptmaps_student_terms', array('id' => $itemid, 'name' => $newvalue));
      // Prepare the element for the output:
      $record->name = $newvalue;
      return new term($record); //new \core\output\inplace_editable('mod_conceptmaps', 'term', $record->id, true,
          //format_string($record->name), $record->name, 'Edit',  'New value for ' . format_string($record->name));
  } else if ($itemtype === 'conceptmaps_edge') {
    \external_api::validate_context(context_system::instance());
    $newvalue = clean_param($newvalue, PARAM_NOTAGS);
    $record = $DB->get_record('conceptmaps_edges', array('connectionid' => $itemid, 'userid' => $USER->id));
    if($record == null) {
      // create new entry or not? At the moment does not save in database !!!! TODO
      return new \core\output\inplace_editable('mod_conceptmaps', 'conceptmaps_edge', $itemid, true,
          $newvalue, $newvalue, new lang_string('editconceptmapterm', 'conceptmaps'),
          new lang_string('newtermfor', 'conceptmaps', $newvalue) );
    } else {
      if($USER->id != $record->userid) {

        // Not allowed, so do not save
        return new \core\output\inplace_editable('mod_conceptmaps', 'conceptmaps_edge', $itemid, true,
            $USER->id, $USER->id, new lang_string('editconceptmapterm', 'conceptmaps'),
            new lang_string('newtermfor', 'conceptmaps', $record->userid));
      }
      // Clean input and update the record.

      $DB->update_record('conceptmaps_edges', array('id' => $record->id, 'content' => $newvalue));
      // Prepare the element for the output:
      $record->name = $newvalue;
      return new \core\output\inplace_editable('mod_conceptmaps', 'conceptmaps_edge', $itemid, true,
          $record->name, $record->name, new lang_string('editconceptmapterm', 'conceptmaps'),
          new lang_string('newtermfor', 'conceptmaps', $record->name));
    }
  }
}
