<?php
# @Date:   2019-11-05T11:40:52+01:00
# @Last modified time: 2019-11-07T15:32:23+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');

require_once(__DIR__.'/classes/controller/view_controller.php');
require_once(__DIR__.'/classes/enums.php');

// Course_module ID, or
$id = optional_param('id', 0, PARAM_INT);

// ... module instance id.
$c  = optional_param('c', 0, PARAM_INT);

if ($id) {
    $cm             = get_coursemodule_from_id('conceptmaps', $id, 0, false, MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('conceptmaps', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($c) {
    $moduleinstance = $DB->get_record('conceptmaps', array('id' => $n), '*', MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm             = get_coursemodule_from_instance('conceptmaps', $moduleinstance->id, $course->id, false, MUST_EXIST);
} else {
    print_error(get_string('missingidandcmid', 'mod_conceptmaps'));
}

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);

$event = \mod_conceptmaps\event\course_module_viewed::create(array(
    'objectid' => $moduleinstance->id,
    'context' => $modulecontext
));
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('conceptmaps', $moduleinstance);
$event->trigger();

$PAGE->set_url('/mod/conceptmaps/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($moduleinstance->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);





$taburl = new moodle_url('/mod/conceptmaps/view.php', array('id' => $id));
$myrenderer = $PAGE->get_renderer('mod_conceptmaps');

$actionstring = optional_param('action', 'show_topics', PARAM_ALPHA); // The default action is 'view'.

$action = Action::get_enum($actionstring);

$controller = new mod_conceptmaps_view_controller($cm->id, $moduleinstance->id);

$controller->handle_access();

echo $OUTPUT->header();

$param = null;
if($action == Action::Topic) {
  require_once(__DIR__.'/classes/forms/edit_topic_form.php');

  $mform = new mod_conceptmaps_edit_topic_form();


  $param = $mform;
}

echo $controller->handle_action($action, $param);

echo $OUTPUT->footer();
